class ModuleChar {
  //how many to be placed
  //location? store but does not self set
  //timing of behavior
  //behavior event
  //updating behavior of some sort?
  //network for detecting neighbor behavior, deciding what to do?
  //set and modify position


  //Graphics Parameters
  int x, y;
  int h;
  String name;
  color onColor = color(206, 239, 221); //color(185, 229, 188);
  color offColor = color(209, 94, 56);
  String text = "";

  //other color variables to set... for instance, for some char emerges as a leader

  //State Parameters
  int state;
  boolean ON = false;
  boolean isSelected = false;
  boolean isUpdated = false;
  boolean preselect = true; //whether this char was selected prior to parameter setting or after
  boolean highlight = false; //allows this char to be highlighted without being "selected"

  //Other Parameters
  float delay = 0;
  int val;
  ModuleChar leader;
  ModuleChar follower;

  int id;//useful for establishing an "order," name, or just add everyone to an array and iterate...


  ModuleChar(int x, int y) {
    this.x = x;
    this.y = y;
    h = 50;
  }

  ModuleChar(int x, int y, String name) {
    this(x, y);
    this.name = name;
  }

  //------------------------------------------Graphics Methods------------------------------------------//

  int[] getCoordinates() {
    return new int[] {x, y};
  }

  int getType(){
    return -1;
  }
  
  int getX() { 
    return x;
  }
  int getY() { 
    return y;
  }

  void setX(int x) { 
    this.x = x;
  }
  void setY(int y) { 
    this.y = y;
  }
  void setXY(int x, int y) {
    this.x = x; 
    this.y=y;
  }

  int getH() {
    return h;
  }

  String name() {
    return this.name;
  }

  void updateCoordinates(int x, int y) { 
    this.x = x; 
    this.y = y;
  }

  void updateState(int s) {
    state = s;
  }

  void setValue(int v) {
    val = v;
  }
  int getValue() {
    return val;
  }

  void setID(int id) {
    this.id = id;
    text += id;
  }

  int getID() {
    return id;
  }

  void setLeader(ModuleChar leader) {
    this.leader = leader;
  }

  void setFollower(ModuleChar follower) {
    this.follower = follower;
  }

  ModuleChar getLeader() {
    return leader;
  }

  ModuleChar getFollower() {
    return follower;
  }

  int getFollowerID() {
    return follower.getID();
  }

  int getLeaderID() {
    return leader.getID();
  }

  void set_preselect(boolean s) {
    preselect = s;
  }
  
  //void drawChar(int on){
  //  drawChar(on, null);
  //}

  boolean drawChar(int on, boolean motion) {
    //screen.beginDraw();


    //screen.noStroke();
    //screen.stroke(150);
    if (LABEL_SHOW){
    screen.fill(255);
    screen.noStroke();
    screen.textSize(15);
    screen.text(text, x, y-10);
    screen.stroke(255);
        }

    //Indicator of whether selected or not
    if (isSelected()) {

      //change color based on whether char before/after parameter setting
      //System.out.println(preselect);

      if (preselect)
        screen.fill(255, 255, 200); //240
      else
        screen.fill(240);

      screen.ellipse(x, y, h+h/8, h+h/8); 
      screen.fill(onColor);
    } else screen.fill(offColor);

    //screen.ellipse(x,y,h,h);
    //screen.endDraw();
    
    return false;
  }


  //------------------------------------------State Methods------------------------------------------//
  boolean isSelected() {
    return isSelected;
  }

  boolean isUpdated() {
    return isUpdated;
  }

  void select() {
    isSelected = !isSelected;
  }

  void select(boolean b) {
    isSelected = b;
  }

  void run() {
  }
  
  void setText(String text){
    this.text = text;
  }


  //class MusicChar extends ModuleChar{

  //  MusicChar(int x, int y) {
  //  this.x = x;
  //  this.y = y;
  //  radius = 10;
  //}

  //}

  //class MvmtChar extends ModuleChar{

  //  MvmtChar(int x, int y){
  //    this.x = x;
  //    this.y = y;
  //    radius = 10;
  //  }
}
