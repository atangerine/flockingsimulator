//places and coordinates module characters, manages the decision making
//1) LTM, etc. ... "stepping" method...
//how often to step in relation to each frame being redrawn (gradual semisteps)

//innovation method

//import beads.*;
//import org.jaudiolibs.beads.*;
import java.util.Map;

class FlockGraph {

  //ModuleChar ms;

  ArrayList<ModuleChar> ms;
  int nodes;
  HashMap<String, Integer> nameMap;
  int size;
  int leader;
  //int[] uedges;
  int[] dedges;
  //MusicPlayer mp;
  //mp = new MusicPlayer(4, 80);
  //int[] followGraph;
  //int[] leadGraph;
  ArrayList<Integer> followGraph;
  ArrayList<Integer> leadGraph;
  ArrayList<List> fsGraph; //leave up to user to not add duplicates or reinforce by using Set
  ArrayList<List> lsGraph;

  FlockGraph() {
    ms = new ArrayList<ModuleChar>();
    nameMap = new HashMap<String, Integer>();
    followGraph = new ArrayList<Integer>();
    leadGraph = new ArrayList<Integer>();

    fsGraph = new ArrayList<List>();
    lsGraph = new ArrayList<List>();

    nodes = 0;
  }

  FlockGraph(int size) {
    this.size = size;
    //uedges = new int[size];
    dedges = new int[size];
    ms = new ArrayList<ModuleChar>();
    nameMap = new HashMap<String, Integer>();
    nodes = 0;
  }

  //Graph g;

  //ModuleChar getNeighbor(){
  //}

  //ModuleChar setNeighbor(){
  //}
  void addChar(ModuleChar m) {
    ms.add(m); //either id# can be assigned here or upon creation of moduleChar
    followGraph.add(-1);
    leadGraph.add(-1);
    fsGraph.add(new ArrayList<Integer>());
    lsGraph.add(new ArrayList<Integer>());
    nameMap.put("", nodes-1);
    m.setID(nodes++);
  }

  void addChar(ModuleChar m, String name) {
    addChar(m);
    nameMap.put(name, nodes-1);
  }

  int getNodes() {
    return nodes;
  }


  void drawGraph(int on) {
    boolean motion = false;
    for (int i = ms.size() - 1; i >= 0; i--) {
      ModuleChar m = ms.get(i);
      motion = m.drawChar(on, motion);
      
     // delay(50);

      if (L_DIR) {
        //add leader vector that points in direction of leaders
        int leader;
        float x0, x1, y0, y1;
        int signx, signy;
        for (int j = 0; j < getLeaders(m.getID()).size(); j++) {
          leader = getLeaders(m.getID()).get(j);

          if (leader != m.getID()) {
            x0 = m.getX() + 10;
            y0 = m.getY() + 20;
            signx = x0 < ms.get(leader).getX()? 1: -1;
            signy = y0 < ms.get(leader).getY()? 1: -1;        
            x1 = x0 + signx*sqrt(abs(ms.get(leader).getX() + 10 - x0))*1.5;
            y1 = y0 + signy*sqrt(abs(ms.get(leader).getY() + 20 - y0))*1.5;

            //System.out.println(ms.get(leader).getX()+10 - x0);
            //line(x0, y0, x1, y1);
            PVector v0 = new PVector(x0,y0);
            PVector v1 = new PVector(x1,y1);
            
            stroke(50);
            drawArrow(v0, v1, 5);
            //System.out.println(tan(((y0-y1)/(x1-x0))));
            //line (x1, y1, x1+10*cos(signx*PI/2.5), y1+10*sin(signy*PI/2.5));
          }
        }
      }
    }
  }


  void drawArrow(PVector v0, PVector v1, int len) {
    
    //float rot = x0 == x1? 0 : tan(((y1-y0)/(x1-x0)));
    line(v0.x,v0.y,v1.x,v1.y);
    
    //if (x1 > x0 && y1 < y0) //1
    //  rot += PI/2;
    //if (x1 < x0 && y1 < y0) //2
    //  rot -= PI/2;
    //if (x1 < x0 && y1 > y0) //3
    //  rot *= -1;
    //if (x1 > x0 && y1 > y0) //4
    //  rot *= -1;
    
    pushMatrix();
    //PVector v = new PVector(x1,y1);
    //rotate(v.heading());
    //translate(v.mag() - len, 0);
    //line(x1,y1,0,-len);
    translate(v1.x,v1.y);
    rotate(atan2(v0.x-v1.x, v1.y-v0.y));
    //translate(v1.mag() - len, 0);
    line(0,0,-len/2,-len);
    
    //line(0, 0,0,len);
    //translate(x1,y1);
    //rotate(PI/6);
    
    
    //line(0, 0, 0, 50);
    popMatrix();
  }

  //void drawArrow(float x, float y, int len, float angle) {
  //  pushMatrix();
  //  translate(x, y);
  //  rotate(radians(angle));
  //  line(0, 0, len, 0);
  //  line(len, 0, len - 8, -8);
  //  line(len, 0, len - 8, 8);
  //  popMatrix();
  //}

  void setOrder(String names[], int[] order) {
  }

  void setOrder(int[] ids, int[] order) {
  }


  //closed = whether leader/follower graph is a closed loop
  int getOrder(boolean closed) {
    if (Collections.frequency(followGraph, -1) > 0) {
      System.out.println("Graph is not connected");
      return -1;
    }

    if (!closed) {
      //find the leader and then look at followers until next follower is negative 1
      ArrayList newOrder = new ArrayList<Integer>(nodes);
      int next = -1;
      int i = 0;
      while (next != 0) {
        i = next;
        next = leadGraph.get(i);
      }

      //not foolproof
      for (int j = 0; j < followGraph.size(); j++) {
        newOrder.add(i); //or I can get the order and traverse the graph in this order... but this might be better
        i = followGraph.get(i);
      }
    }
    //set old order to this new order

    return 0;
  }

  //type = directed = "d" or undirected = "u"
  //void addEdge(int from, int to) {
  //}

  //following are for single leader/follower relationships

  void addEdge(String a, String b) {
    int n1 = nameMap.get(a);
    int n2 = nameMap.get(b);
    ms.get(n2).setFollower(ms.get(n1));
    ms.get(n1).setLeader(ms.get(n2));

    //setLeader(n1, n2);
    //setFollower(n1, n2);
    //followMap[n2] = n1;
    //leadMap[n1] = n2;
  }

  void addEdge(int n1, int n2) {
    ms.get(n2).setFollower(ms.get(n1));
    ms.get(n1).setLeader(ms.get(n2));
    setLeader(n1, n2);
    setFollower(n1, n2);
  }

  void sortOrder() {
    //travel until no leader
    //successively until no follower
  }

  void setLeader(int follower, int leader) {
    leadGraph.set(follower, leader);
  }

  void setFollower(int follower, int leader) {
    followGraph.set(leader, follower);
  }

  int getLeader(int follower) {
    return leadGraph.get(follower);
  }

  int getFollower(int leader) {
    return followGraph.get(leader);
  }


  //following are for multiple follower/leader relationships
  void addFollower(int leader, int follower) {
    if (!fsGraph.get(leader).contains(follower))
      fsGraph.get(leader).add(follower);
  }

  ArrayList<Integer> getFollowers(int leader) {
    return (ArrayList<Integer>)fsGraph.get(leader);
  }

  void addLeader(int leader, int follower) {
    if (!lsGraph.get(follower).contains(leader))
      lsGraph.get(follower).add(leader);
  }

  ArrayList<Integer>  getLeaders(int follower) {
    return (ArrayList<Integer>)lsGraph.get(follower);
  }



  void clearAll() {

    ms.clear();
    nameMap.clear();
    followGraph.clear();
    leadGraph.clear();
  }
}

//  int[] getLeadMap() {
//    return leadMap;
//  }

//  int[] getFollowMap() {
//    return followMap;
//  }
//}
//return ordered list of chars for iteration

//main calls flockgraph, which contains modulechars, which can be either mvmtChar or musicChar
