import controlP5.*;
import java.util.*;

//import beads.*;
//import org.jaudiolibs.beads.*;
import java.util.Map;
import java.util.Calendar.*;

//ControlP5 cp5;


//control panel variables
PGraphics menu;

//screen variables
PGraphics screen;
int screenType;
color screenColor;

int SC_OPT = 0;
//int MVMT_DEF = 0;
//int MVMT_PLACE = 1; just use PD_OPT
int MVMT = 0;
int MUSIC = 2; 

float OFFX_MVMT, OFFY_MVMT;
boolean GLOBAL = true;
Group g0, g1, g2;

int width_i, height_i;
int sWidth, sHeight;
int w_units, h_units;

ScrollableList mm_mode, save_options;

//Movement Control
RadioButton r1, r_lf;
Toggle r_pd, r_lock, show_label, ldir_label, r_r, stats_label;
Button ds, clr, rem, exit_out, screen_shot;
ScrollableList l_rem;
Textlabel rlf_label, labeling, m_input;
Textfield name_label;
Textfield state_input;

Textarea myTextarea;
Println console;

//Music Control
Knob tempoKnob;
CheckBox activeModeSet;
Slider s_strict;
int RELAX = 0;
boolean STRICT_LESS = false;
boolean dragged = false;
int TIME = 0;

//GUI Design Elements
PFont font, cfont, labelFont_l, labelFont_s;
int cw_units = (width - sWidth)/100;
int ch_units;
int bw_unit;
int bh_unit;
int l_space;
int m_space;
int s_space;
int opacity = 30;


//MOVEMENT variables
int ON = 0;
int OFF = 1;
int LEAD_OPT = 0;
int FOLLOW_OPT = 1;
int LF_OPT = -1;
int LF_OPT_LAST = -1;
int lf_selected;

int PD_OPT = 0;
int DEF_OPT = 0;
int PLACE_OPT = 1;


boolean PD_LOCK = false;
boolean SELECT_LOCK = false;
boolean LABEL_SHOW = true;
boolean L_DIR = false;
boolean SHOW_STATS = false;

int LAYER_MODE = -1;


//GENERAL MODULE VARIABLES
FlockGraph f;
ArrayList<FlockGraph> fm, fp;
int numChars = 0;
ArrayList<List> selected_chars;
ArrayList<List> layerlist;


//MUSIC control variables
MusicChar[] mcs = new MusicChar[4];
int LEADER = 0;
int numSelected = 0;
int char_selected = -1;

//representative of "starting state"
//set it as default in the beginning and allow user to chnage
String[][] _phrases = {
  {"D4", "C4", "D4"}, 
  {"D4", "C4", "D4"}, 
  {"D4", "C4", "D4"}, 
  {"D4", "C4", "D4"}
  //{"E4", "D4", "E4"}, 
  //{"F4", "E4", "F4"},
  //{"G4", "F4", "G4"},
};
float[][] _durations = {
  {0.25, 0.25, 0.5}, //{0.25, 0.25, 3.5}, 
  {0.25, 0.25, 0.5}, //{0.25, 0.25, 3.5}, 
  {0.25, 0.25, 0.5}, 
  {0.25, 0.25, 0.5}
};
float[][] _starts = {
  {0, 0.25, 0.5}, 
  {0, 0.25, 0.5}, 
  {0, 0.25, 0.5}, 
  {0, 0.25, 0.5}
  //{2, 2.25, 2.5}, 
  //{3, 3.25, 3.5}, 
  //{0, 0.25, 0.5}, 
  //{1, 1.25, 1.5}
};

float[] _offsets = {0, 1, 2, 3}; // beats of rest before initiating
float[] _off = {0, 1, 2, 3};

int[] repeats = {4, 4, 4, 4};

//strict wavelet features
int NUM_FEATURES = 4;

int RISE_FALL = 0;
int OPEN_CLOSED = 1;
int SOAR_BEAT = 2;
int BIG_SMALL = 3;

boolean RISE = true;
boolean OPENED = true;
boolean SOAR = true;
boolean BIG = true;

boolean FALL = false;
boolean CLOSED = false;
boolean BEAT = false;
boolean SMALL = false;

boolean[][] _modes = {
  {FALL, CLOSED, BEAT, SMALL}, 
  {FALL, CLOSED, BEAT, SMALL}, 
  {FALL, CLOSED, BEAT, SMALL}, 
  {FALL, CLOSED, BEAT, SMALL}
};

List<String[]> phrases;
List<float[]> durations;
List<float[]> starts;
List<boolean[]> modes;

//animation control variables
boolean RELOAD = false;
boolean RUN = false;
boolean PAUSE = true;
float FRAMERATE= 1;
int WTIME = 4;
boolean PRESELECT = true;
int CLEAR_SELECTED = -1;
int PRESELECT_SIZE = 0;

//Design Formatting variables
int TOP_BORDER;
int LEFT_BORDER;

//String "module" = "Module Type";


//Audio
int VOICES = 4;
Minim minim;
AudioOutput[] out = new AudioOutput[VOICES];
int UPDATE = -1; //next mc to update
int FLIP = -2; //mc that sets off wave
int CHANGE_DELAY = 0;
int tempo = 60;

boolean[] activeModes;

//-----------------------------------SETUP-----------------------------------//

void setup() {
  //size(2500, 1500);
  fullScreen();
  surface.setResizable(true);
  w_units = width/20;
  h_units = height/20;
  TOP_BORDER = height/50;
  LEFT_BORDER = TOP_BORDER;

  sWidth = (int)(width/1.5);
  sHeight = height - TOP_BORDER*2;
  OFFX_MVMT = sWidth/4.5;
  OFFY_MVMT = sWidth/8;
  //width_i = 2500;
  //height_i = 1500;

  screenType = MVMT;
  screen = createGraphics(sWidth, sHeight);

  cp5 = new ControlP5(this);
  cp5.setAutoDraw(false);

  List l = Arrays.asList("music", "movement");

  //track which moduleChars are currently selected
  //selected_chars.get(MUSIC) = new ArrayList<Integer>();
  //selected_chars.get(SC_OPT) = new ArrayList<Integer>();

  //for movement and music selected
  selected_chars = new ArrayList<List>(3);
  for (int i = 0; i < 3; i++) {
    selected_chars.add(new ArrayList<Integer>());
  }

  layerlist = new ArrayList<List>(10);
  for (int i = 0; i < 10; i++) {
    layerlist.add(i, new ArrayList<Integer>());
  }

  //relative sizing
  cw_units = (width - sWidth)/100;
  ch_units = height/100;
  bw_unit = cw_units*3;
  bh_unit = ch_units/2;
  l_space = bw_unit*3;
  m_space = bw_unit*2;
  s_space = bw_unit;

  font = createFont("calibri", bh_unit*2.5);
  cfont = createFont("calibri", bh_unit*2.5);
  labelFont_s = createFont("Georgia", 15);
  labelFont_l = createFont("Georgia", 32);


  g0 = cp5.addGroup("g0")
    .setPosition(sWidth, 0)
    .setWidth(width - sWidth - LEFT_BORDER)
    .activateEvent(true)
    //.setBackgroundColor(color(255, 80))
    .setBackgroundHeight(sHeight)
    .setBarHeight(40)
    .setLabel("Menu")
    .setFont(font)
    .show()
    ;


  //which module we're running
  mm_mode = cp5.addScrollableList("module")
    .setPosition(cw_units*10, TOP_BORDER)
    .setSize(bw_unit*10, bh_unit*25)
    .setBarHeight(bw_unit*2)
    .setItemHeight(bw_unit*2)
    .addItems(l)
    .setType(ScrollableList.LIST) // or module
    .setFont(font)
    .setOpen(true)
    .setGroup(g0)
    ;

  //whether we're running the simulation
  r_r = cp5.addToggle("runToggle")
    .setValue(0)
    .setCaptionLabel("Run")
    .setPosition(cw_units*60, TOP_BORDER)
    .setSize(bw_unit * 6, bh_unit*6)
    .setFont(font)
    .setMode(0)
    .setGroup(g0)
    ;

  r_r.getCaptionLabel().setColor(color(80, 120, 150));


  //when place is selected, draw from separate set of characters, add to this set as user clicks, selecting when place is ON/OFF --> 
  //can drag character around
  //only when place selected but OFF can characters be selected and dragged, dragging is off when RUN toggle is ON

  r_pd = cp5.addToggle("placeToggle")
    .setValue(0)
    .setCaptionLabel("P                 D")
    .setPosition(cw_units*60, TOP_BORDER+r_r.getHeight() + m_space)
    .setSize(bw_unit * 6, bh_unit*6)
    .setFont(font)
    .setMode(ControlP5.SWITCH)
    .setGroup(g0)
    ;
  r_pd.getCaptionLabel().setColor(color(80, 120, 150));

  r_lock = cp5.addToggle("lockToggle")
    .setValue(0)
    .setCaptionLabel("Lock")
    .setPosition(cw_units*60, TOP_BORDER + r_r.getHeight() + r_pd.getHeight() + m_space*2)
    .setSize(bw_unit * 3, bh_unit*6)
    .setFont(font)
    .setMode(0)
    .setGroup(g0)
    ;

  r_lock.getCaptionLabel().setColor(color(80, 120, 150));

  //run vs "animate" button, run = run behavior propagation, animate = xy coordinate modifications

  //List rl = Arrays.asList("Remove", "Deselect All", "Clear All");
  //  //which module we're running
  //l_rem = cp5.addScrollableList("removing")
  //  .setPosition(sWidth + 100, 250)
  //  .setSize(200, 40)
  //  .setBarHeight(height/40)
  //  .setItemHeight(height/40)
  //  .addItems(rl)
  //  .setType(ScrollableList.LIST) // or module
  //  .setFont(font)
  //  .setOpen(true)
  //  ;

  //button for deselecting all selected
  ds = cp5.addButton("deselect")
    .setPosition((width-sWidth - LEFT_BORDER)*10/100, mm_mode.getHeight()+ m_space)
    .setSize(bw_unit * 8, bh_unit*7)
    .setCaptionLabel("Deselect All")
    .setFont(font)
    .setGroup(g0)
    ;


  rem = cp5.addButton("removes")
    .setPosition((width-sWidth - LEFT_BORDER)*10/100, ds.getPosition()[1] + ds.getHeight() + s_space)
    .setSize(bw_unit * 8, bh_unit*7)
    .setCaptionLabel("Remove")
    .setFont(font)
    .setGroup(g0)
    ;

  ////button for clearing all in place mode
  //clr = cp5.addButton("clearall") //MUST NOT USE KEY WORD OR ELSE "black bg" behavior
  //  .setPosition(rem.getPosition()[0], rem.getPosition()[1] + rem.getHeight() + s_space)
  //  .setSize(bw_unit * 8, bh_unit*7)
  //  .setCaptionLabel("Clear All")
  //  .setFont(font)
  //  .setGroup(g0)
  //  ;

  //exit without pressing esc
  exit_out = cp5.addButton("exit")
    .setPosition((width-sWidth)-bw_unit*2, 0)
    .setColorBackground(color(173, 12, 12))
    .setSize(bw_unit*2, bw_unit*2)
    .setCaptionLabel("X")
    .setFont(font)
    .setGroup(g0)
    ;

  List save_l = Arrays.asList("Screenshot", "W/O CTRLs", "JSON");
  //which module we're running
  //save_options = cp5.addScrollableList("save_options")
  //  .setPosition((width-sWidth - LEFT_BORDER)*10/100 + clr.getWidth() + m_space, sHeight+TOP_BORDER-bh_unit*7)
  //  .setSize(bw_unit*10, bh_unit*35)
  //  .setBarHeight(bh_unit*7)
  //  .setItemHeight(bw_unit*2)
  //  .addItems(save_l)
  //  .setType(ScrollableList.LIST) // or module
  //  .setCaptionLabel("Save")
  //  .setFont(font)
  //  .setOpen(false)
  //  .setGroup(g0)
  //  ;

  //save_options.getCaptionLabel().setPaddingX(bw_unit).setPaddingY(bh_unit);//align(ControlP5.CENTER, ControlP5.CENTER).setPaddingX(bw_unit/4);
  //  mm_mode = cp5.addScrollableList("module")
  //.setPosition(cw_units*10, TOP_BORDER)
  //.setSize(bw_unit*10, bh_unit*25)
  //.setBarHeight(bw_unit*2)
  //.setItemHeight(bw_unit*2)
  //.addItems(l)
  //.setType(ScrollableList.LIST) // or module
  //.setFont(font)
  //.setOpen(true)
  //.setGroup(g0)
  //;

  screen_shot = cp5.addButton("Screenshot")
    .setPosition((width-sWidth - LEFT_BORDER)*10/100, sHeight+TOP_BORDER-bh_unit*7)
    .setSize(bw_unit * 8, bh_unit*7)
    .setCaptionLabel("Save")
    .setFont(font)
    .setColorBackground(color(173, 12, 12))
    .setGroup(g0)
    ;

  //exit_save = cp5.addButton("exit")
  //.setPosition((width-sWidth - LEFT_BORDER)*10/100 + clr.getWidth() + m_space, sHeight+TOP_BORDER-bh_unit*7)
  //.setSize(bw_unit * 8, bh_unit*7)
  //.setCaptionLabel("Exit/Save")
  //.setFont(font)
  //.setGroup(g0)
  //;

  //cp5.addTextfield("numCharacters")
  //  .setCaptionLabel("# Chars")
  //  .setPosition(100, 270)
  //  .setSize(200, 40)
  //  .setFont(font)
  //  .setAutoClear(false)
  //  .setGroup(g0)
  //  ;

  //extra feature - taking screenshot of just "screen" portion - could just screenshot though

  //cp5.addSlider("# Chars")
  //  .setPosition(sWidth+100, 200)
  //  .setSize(200, 50)
  //  .setRange(0, 24) 
  //  .setFont(font)
  //  .setNumberOfTickMarks(6)
  //  ;


  //adding this group to a Tab... showing tab based on which module is selected... might be temporary fix
  //like group but get values for group by pulling from selected ModuleChar
  //1) Set leader/follower -> leader - click and choose followers and vice versa
  //-if multiple chosen, control flow - modify ON vs choose on/off
  //control flow for what motion is stopped - when CHOOSE if on
  //2) multi-layer networks/formulae for considering input from various layers -- 
  //different highlighting colors - chosen one X and relevant ones Y - can use leader/follower color scheme
  //3) Can take images/derive strings that move, converting one image into another on the basis of chars
  //4) 

  //----------CONTROL CHARACTERS GUI GROUP------------------//
  //ControlFont cf1 = new ControlFont(createFont("Roboto", 50));
  //ControlFont cf2 = new ControlFont(createFont("Arial", 18));


  g1 = cp5.addGroup("g1")
    .setPosition((width-sWidth - LEFT_BORDER)*10/100, rem.getPosition()[1] + rem.getHeight() + l_space*2)
    .setWidth((width-sWidth)*8/10)
    .activateEvent(true)
    .setBackgroundColor(color(255, 80))
    .setBackgroundHeight(sHeight/4)
    .setBarHeight(40)
    .setLabel("Music Module")
    .setFont(font)
    .setGroup(g0)
    ; 

  g1.hide();



  g2 = cp5.addGroup("g2")
    .setPosition(g1.getPosition()[0], g1.getPosition()[1]) //(width-sWidth - LEFT_BORDER)*10/100, g1.getPosition()[1] + g1.getBackgroundHeight() + l_space + m_space
    .setWidth((width-sWidth)*8/10)
    .activateEvent(true)
    .setBackgroundColor(color(255, 80))
    .setBackgroundHeight(sHeight/4)
    .setBarHeight(40)
    .setLabel("Movement Module")
    .setFont(font)
    .setGroup(g0)
    ;


  myTextarea = cp5.addTextarea("txt")
    .setPosition((width-sWidth - LEFT_BORDER)*10/100, g1.getPosition()[1] + g1.getBackgroundHeight() + l_space + m_space)
    .setSize((width-sWidth)*8/10, sHeight/4)
    .setFont(font)
    .setLineHeight(20)
    .setColor(color(255))
    .setColorBackground(color(0, 100))
    .setColorForeground(color(255, 100))
    .setGroup(g0);

  console = cp5.addConsole(myTextarea);//

  //might be nice to have a console that displays data


  //g2.enableCollapse();

  tempoKnob = cp5.addKnob("Tempo")
    .setRange(60, 255)
    .setValue(tempo)
    .setPosition(g1.getWidth()/15, g1.getBackgroundHeight()/10)
    .setRadius(g1.getWidth()/6)
    .setDragDirection(Knob.VERTICAL)
    .setNumberOfTickMarks(10)
    .setTickMarkLength(bh_unit/2)
    .setGroup(g1)
    .setLabel("Tempo")
    .setFont(font)
    ;
  //g2.hide();
  tempoKnob.getCaptionLabel().setColor(color(80, 120, 150));
  //tempoKnob.setShowRange(true);

  activeModeSet = cp5.addCheckBox("Active Modes")
    .setPosition(tempoKnob.getRadius()*2 + g1.getWidth()*2/10, g1.getBackgroundHeight()/10)
    .setSize(g1.getWidth()/22, g1.getWidth()/22)
    //.setColorForeground(color(120))
    //.setColorActive(color(150, 200, 150))
    //.setColorLabel(color(255))
    .setSpacingRow(g1.getBackgroundHeight()/20)
    .addItem("RISE/FALL", 0)
    .addItem("OPEN/CLOSED", 1)
    .addItem("BEAT/SOAR", 2)
    .addItem("BIG/SMALL", 3)
    .setCaptionLabel("Active Modes")
    //.setFont(font)
    .setGroup(g1)
    ;


  s_strict = cp5.addSlider("RELAXNESS")
    .setPosition(activeModeSet.getPosition()[0], activeModeSet.getPosition()[1] + activeModeSet.getHeight()*20 + m_space)
    .setSize(g1.getWidth()/4, g1.getWidth()/16)
    .setRange(0, 10)
    .setNumberOfTickMarks(11)
    .setValue(RELAX)
    .setGroup(g1)
    .setFont(font)
    .setColorTickMark(50)
    .snapToTickMarks(true)
    .setSliderMode(Slider.FLEXIBLE)
    //.listen(true)
    .setBroadcast(true)
    .setMoveable(true)
    ;

  cp5.getController("RELAXNESS").getCaptionLabel()
    .align(ControlP5.LEFT, ControlP5.BOTTOM_OUTSIDE).setPaddingY(s_space/2)
    .setColor(color(80, 120, 150));


  for (Toggle t : activeModeSet.getItems()) {
    //t.getCaptionLabel().setColorBackground(color(50,80,100));
    t.getCaptionLabel().setFont(font);
    t.getCaptionLabel().setColor(color(80, 120, 150));
    //t.getCaptionLabel().setFont(cf2);
    //t.getCaptionLabel().getStyle().moveMargin(-7, 0, 0, -3);
    //t.getCaptionLabel().getStyle().movePadding(7, 0, 0, 3);
    //t.getCaptionLabel().getStyle().backgroundWidth = 45;
    //t.getCaptionLabel().getStyle().backgroundHeight = 13;
  }

  //cp5.addSlider("S-1")
  //  .setPosition(80, 10)
  //  .setSize(180, 9)
  //  .setGroup(g2)
  //  ;

  //cp5.addSlider("S-2")
  //  .setPosition(80, 20)
  //  .setSize(180, 9)
  //  .setGroup(g2)
  //  ;
  //RadioButton r1 = cp5.addRadioButton("radio")
  //  .setPosition(20,20)
  //  .setSize(50,30)
  //  .addItem("black", 0)
  //  .addItem("red", 1)
  //  .addItem("green", 2)
  //  .addItem("blue", 3)
  //  .addItem("grey", 4)
  //  .setCaptionLabel("black")
  //  .setGroup(g2)
  //  .setSpacingRow(10)
  //  ;

  //   for(Toggle t:r1.getItems()) {
  //     t.getCaptionLabel().setColorBackground(color(255,80));
  //     t.getCaptionLabel().getStyle().moveMargin(-7,0,0,-3);
  //     t.getCaptionLabel().getStyle().movePadding(7,0,0,3);
  //     t.getCaptionLabel().getStyle().backgroundWidth = 45;
  //     t.getCaptionLabel().getStyle().backgroundHeight = 13;
  //   }


  //different layers of information/parameters, button to show chars involved in each layer
  //when layer option chosen and char selected, char added to chars involved in layer, can set parameters
  //maybe additional sliders etc to set
  int g2w_unit = g2.getWidth()/20;
  int g2h_unit = g2.getBackgroundHeight()/20;
  PFont g2_font = createFont("calibri", g2.getBackgroundHeight()/20);
  PFont g2_heading = createFont("calibri", g2.getBackgroundHeight()/16);

  r1 = cp5.addRadioButton("radioButton")
    .setPosition(g2w_unit, g2h_unit*4)
    .setSize(g2w_unit, g2h_unit)
    .setColorForeground(color(120))
    .setColorActive(color(150, 200, 150))
    .setColorLabel(color(255))
    .setSpacingRow(g2h_unit)
    .addItem("black", 0)
    .addItem("red", 1)
    .addItem("green", 2)
    .addItem("blue", 3)
    .addItem("grey", 4)
    .setCaptionLabel("Layer Ctrl")
    //.setFont(g2_heading)
    .setGroup(g2)
    ;

  cp5.addTextlabel("r1_label")
    .setText("Layer Ctrl")
    .setPosition(g2w_unit*0.8, g2h_unit*1.5)
    .setGroup(g2)
    .setFont(g2_heading)
    .setColorValue(color(150, 120, 150))
    ;

  rlf_label = cp5.addTextlabel("rlf_label")
    .setText("LF Ctrl")
    .setPosition(g2w_unit*5.3, g2h_unit*1.5)
    .setGroup(g2)
    .setFont(g2_heading)
    .setColorValue(color(150, 120, 150))
    ;
  rlf_label.hide();

  r_lf = cp5.addRadioButton("LFButton")
    .setPosition(g2w_unit*5.5, g2h_unit*4)
    .setSize(g2w_unit*3/2, g2h_unit)
    .setColorForeground(color(120))
    .setColorActive(color(150, 200, 150))
    .setColorLabel(color(255))
    .setSpacingRow(g2h_unit)
    //.addItem("leader", 0)
    //.addItem("follower", 1)
    .addItem("show Ls", 0)
    .addItem("show Fs", 1)
    .setCaptionLabel("LF Ctrl")
    //.setFont(g2_heading)
    .setGroup(g2)
    ;


  labeling = cp5.addTextlabel("labeling")
    .setText("Labels")
    .setPosition(g2w_unit*10.5, g2h_unit*1.5)
    .setGroup(g2)
    .setFont(g2_heading)
    .setColorValue(color(150, 120, 150))
    ;

  fill(80, 120, 150);


  ldir_label = cp5.addToggle("ldir_label")
    .setValue(0)
    .setCaptionLabel("L Dir")
    .setColorActive(color(150, 200, 150))
    .setPosition(g2w_unit*10.7, g2h_unit*4)
    .setFont(g2_font)
    .setSize(g2h_unit*2, g2h_unit)
    .setGroup(g2)
    ;

  show_label = cp5.addToggle("show_label")
    .setValue(1)
    .setCaptionLabel("Show IDs")
    .setColorActive(color(150, 200, 150))
    .setPosition(g2w_unit*10.7, g2h_unit*7)
    .setFont(g2_font)
    .setSize(g2h_unit*2, g2h_unit)
    .setGroup(g2)
    ;

  stats_label = cp5.addToggle("stats_label")
    .setValue(0)
    .setCaptionLabel("Stats")
    .setColorActive(color(150, 200, 150))
    .setPosition(g2w_unit*10.7, g2h_unit*10)
    .setFont(g2_font)
    .setSize(g2h_unit*2, g2h_unit)
    .setGroup(g2)
    ;


  m_input = cp5.addTextlabel("Input")
    .setText("Input")
    .setPosition(g2w_unit*14, g2h_unit*1.5)
    .setGroup(g2)
    .setFont(g2_heading)
    .setColorValue(color(150, 120, 150))
    ;

  //allow user to enter name
  name_label = cp5.addTextfield("Name")
    .setPosition(g2w_unit*14, g2h_unit*8)
    .setSize(g2w_unit*4, g2h_unit*2)
    .setFont(g2_font)
    .setFocus(true)
    //.setColor(color(80, 120, 150))
    .setGroup(g2)
    ;

  state_input = cp5.addTextfield("State")
    .setPosition(g2w_unit*14, g2h_unit*4)
    .setSize(g2w_unit*4, g2h_unit*2)
    .setFont(g2_font)
    .setFocus(true)
    //.setColor(color(80, 120, 150))
    .setGroup(g2)
    ;


  state_input.getCaptionLabel().setColor(color(80, 120, 150));
  stats_label.getCaptionLabel().setColor(color(80, 120, 150));
  name_label.getCaptionLabel().setColor(color(80, 120, 150));
  show_label.getCaptionLabel().setColor(color(80, 120, 150));
  ldir_label.getCaptionLabel().setColor(color(80, 120, 150));

  name_label.hide();
  //labeling.hide();
  //show_label.hide();




  r_lf.hide();



  //r_lf.setTitle("LF Ctrl");
  //r_lf.hide();

  System.out.println(r1.getCaptionLabel().toString());

  for (Toggle t : r1.getItems()) {
    //t.getCaptionLabel().setColorBackground(color(50,80,100));
    t.getCaptionLabel().setFont(font);
    t.getCaptionLabel().setColor(color(80, 120, 150));
    //t.getCaptionLabel().setFont(cf2);
    //t.getCaptionLabel().getStyle().moveMargin(-7, 0, 0, -3);
    //t.getCaptionLabel().getStyle().movePadding(7, 0, 0, 3);
    //t.getCaptionLabel().getStyle().backgroundWidth = 45;
    //t.getCaptionLabel().getStyle().backgroundHeight = 13;
  }

  for (Toggle r : r_lf.getItems()) {
    //t.getCaptionLabel().setColorBackground(color(50,80,100));
    r.getCaptionLabel().setColor(color(80, 120, 150));
    r.getCaptionLabel().setFont(font);
    //r.getCaptionLabel().getStyle().moveMargin(-7, 0, 0, -3);
    //r.getCaptionLabel().getStyle().movePadding(7, 0, 0, 3);
    //r.getCaptionLabel().getStyle().backgroundWidth = 45;
    //r.getCaptionLabel().getStyle().backgroundHeight = 13;
  }




  //this set only appears when ONE char selected
  //radio button Pick Followers -->
  //radio button Pick Leaders --> ones selected next will be set as followers


  //which innovation method

  screenColor = color(152, 210, 218);
  MusicChar s = new MusicChar(sWidth/7, height*2/10, "s");
  MusicChar a = new MusicChar(sWidth/7, height*4/10, "a");
  MusicChar t = new MusicChar(sWidth/7, height*6/10, "t");
  MusicChar b = new MusicChar(sWidth/7, height*8/10, "b");

  //set starting phrases
  //m1.setPhrase(new String[]{"C", "D", "E"});
  //m2.setPhrase(new String[]{"D", "E", "F"});
  //m3.setPhrase(new String[]{"E", "F", "G"});
  //m4.setPhrase(new String[]{"F", "G", "A"});


  //set up flockgraph for analysis
  f = new FlockGraph();
  fm = new ArrayList<FlockGraph>(2);
  fm.add(0, new FlockGraph());
  fm.add(1, new FlockGraph());
  //fm.get(DEF_OPT) = new FlockGraph();
  //fp = new FlockGraph();


  //set which modes are active
  activeModes = new boolean[4];
  activeModes[RISE_FALL] = true;
  activeModes[OPEN_CLOSED] = true;
  activeModes[SOAR_BEAT] = true;
  activeModes[BIG_SMALL] = true;

  float[] fam = {1, 1, 1, 1};

  activeModeSet.setArrayValue(fam);

  f.addChar(s, "s");
  f.addChar(a, "a");
  f.addChar(t, "t");
  f.addChar(b, "b");

  //strict following order



  //set leader/follower relationships (directed edges)
  f.addEdge("s", "b"); //s follows b 
  f.addEdge("b", "t"); // s<-a<-t<-b<-s
  f.addEdge("t", "a");
  f.addEdge("a", "s");



  //addEdges currently adds leader/follower relationship  

  //strict/loose waveguide model
  //pulsed, closed following order (loop with established leader)
  //determined following time (this for now)
  //strict wavelet copy, flip
  //


  for (ModuleChar m : f.ms) {
    m.setValue((int)(random(0, 50)));
  }

  //MusicPlayer muse = new MusicPlayer(1,90);
  minim = new Minim(this);
  for (int i = 0; i < VOICES; i++) out[i] = minim.getLineOut();

  //Volume Controller must be created
  //out[0].setVolume(0.01);
  //out[1].setVolume(0.3);


  //Set initial values for MUSIC MODULE if no values given through GUI
  //How to set: before running, create and click on each character, enter information when character selected

  //Soprano
  //m1.setPhrase(new String[] {"D","C","D"});
  //m1.setDurations(new float[] {0.25,0.25,3.5});
  //m1.setStarts(new float[] {2,2.25,2.5});

  ////Alto
  //m.setPhrase(new String[]);
  //m.setDurations(new float[]);
  //m.setStarts(new float[]);

  ////Tenor
  //m.setPhrase(new String[]);
  //m.setDurations(new float[]);
  //m.setStarts(new float[]);

  //Bass

  phrases = new ArrayList<String[]>();
  durations = new ArrayList<float[]>();
  starts = new ArrayList<float[]>();
  modes = new ArrayList<boolean[]>();

  for (int i = 0; i < 4; i++) {
    phrases.add(_phrases[i]);
    durations.add(_durations[i]);
    starts.add(_starts[i]);
    modes.add(_modes[i]);
  }

  for (int i = 0; i < f.getNodes(); i++) {
    MusicChar m = (MusicChar)f.ms.get(i);
    m.getModel().setModes(_modes[i]);
    m.setPhrase(phrases.get(i));
    m.setLength(phrases.get(i).length);
    m.setDurations(durations.get(i));
    m.setStarts(starts.get(i));
    m.setRepeat(repeats[i]);
    mcs[i] = m;

    int[] nums = {42, 40, 42};
    m.setPhraseNums(nums);
  }


  //------------------------------------------MAKE THE ARRAY OF MOVEMENT CHARACTERS------------------------------------------///

  //one graph for default and one graph for placed
  int nw = 10;
  int nh = 10;
  int w_inc = sWidth/15;
  int h_inc = sHeight/15;
  int left = (sWidth - (nw-1)*w_inc)/2;
  int top = (sHeight - (nh-1)*h_inc)/2;
  for (int i = 0; i < 10; i++) {
    for (int j =0; j < 10; j++) {
      fm.get(DEF_OPT).addChar(new MvmtChar((int)left  + i*w_inc, (int)top + j*h_inc));
    }
  }

  //establish sequential follower/leader connections
  for (int i = 0; i<99; i++) {
    //fm.get(DEF_OPT).setFollower(i, i+1);
    //fm.get(DEF_OPT).setLeader(i+1, i);
    fm.get(DEF_OPT).addFollower(i, i+1);
    fm.get(DEF_OPT).addLeader(i, i+1);
    fm.get(DEF_OPT).ms.get(i).setLeader(fm.get(DEF_OPT).ms.get(i+1));
    fm.get(DEF_OPT).ms.get(i+1).setFollower(fm.get(DEF_OPT).ms.get(i));
  }
}



//--------------------------DRAW FUNCTION-----------------------------------------//

//--------------------------DRAW FUNCTION-----------------------------------------//
float ftime = 0;
void draw() {
  //g0.hide();
  //surface.setSize(width, height);
  //scale(width/width_i, height/height_i);

  //System.out.println("Player " + ftime);

  //background(150,150,150,30);
  //background(200,30);


  //change frame rate based on type of simulation run

  //start drawing to the display screen (not control panel area)
  screen.beginDraw();


  //set up the appropriate background
  if (screenType == MUSIC) {
    musicScreen(opacity);
    opacity = 10;
    //if (frameCount % 200 == 0) opacity = 150;
    //g2.setVisible(false);//hide();
    //g1.setVisible(true);
    //g2.hide();
    g2.hide();
    g1.show();
    console.pause();
    cp5.draw();
    //console.play();
  } else if (screenType == MVMT) {
    //g1.hide();
    g1.hide();
    g2.show();
    console.play();
    cp5.draw();

    mvmtScreen();
  }

  //TBD: if not PAUSEd, update everything in FlockGraph according to the rules
  //might merge PAUSE and RUN


  //text(cp5.get(Textfield.class,"# Chars").getText(), 360,130);

  //----------------------------MUSIC MODE ANIMATION----------------------------//

  //if we want to increase FRAMERATE, we only execute below on certain %time values


  //where to store offset, length, phrase, and repeat data?
  //most efficient way to call
  //anything to prevent drift? where's drift coming from 
  //only get again if marked as changed

  //AUDIO PROCESSING PORTION

  //BELOW ACTIONS RELATIVE TO FRAMERATE = 1

  //for (int i = 0; i < out.length; i++) {
  //  for (int j = 0; j < phrases[i].length; j++) {
  //    //if ((int)random(3) == 0) {p = "F5";}
  //    out[i].playNote(starts[i][j] + (repeats[i]-1)*time - 0.01*(VOICES-j), durations[i][j], phrases[i][j]); //0.01*j as epsilon*correcting factor
  //    //System.out.println(durations[i][j] + 4*time);
  //  }
  //}


  //figure out... how much to pause?
  //track changes made throughout 
  //if (screenType == MUSIC && ftime < 1) { 
  //  f.drawGraph(ON);
  //}


  //main challenge seems to be timing visuals/audio

  if (!RUN && screenType == MUSIC) for (int i = 0; i < out.length; i++) {
    out[i].pauseNotes(); //pause music immediately
    ((MusicChar)f.ms.get(i)).drawChar(0);
  }

  if (RUN && screenType == MUSIC) { //otherwise, run the sound simulation
    frameRate(10*tempo/60); //tempo/60

    if (frameCount % 10 == 0) {

      for (int i = 0; i < out.length; i++) {
        out[i].pauseNotes();
      }


      //updateMusic(); //update according to wavelength model rules by Dan Trueman

      //schedule all the notes
      for (int i = 0; i < out.length; i++) {

        MusicChar m = ((MusicChar)f.ms.get(i));

        //MusicChar m2 = ((MusicChar)f.ms.get((i+1)%4));
        System.out.println("Player " + i + " " + ftime + " " + _offsets[i]);
        //only schedule when about to end
        //schedule, if note is supposed to be scheduled this round
        if (_offsets[i] == 0)//&& (int)ftime % 2 == 0) 
        {



          // - 0.01*(VOICES-j
          if (_offsets[i] == 0) {

            if (CHANGE_DELAY <= 0 && (int)random(12) < 4) {
              screen.text("Update!", 30, 30);
              updateMusic(i);
            }
            CHANGE_DELAY--;
            String[] p = ((MusicChar)f.ms.get(i)).getPhrase();
            int len = ((MusicChar)f.ms.get(i)).getLength();
            for (int j = 0; j < len; j++) {
              out[i].playNote(starts.get(i)[j], m.getDuration(j), p[j]); //durations.get(i)[j] //when "update" can't add ftime...
              out[i].setTempo(tempo);
              //out[(i+1)%4].playNote(starts.get((i+1)%4)[j] + ftime + 1, m2.getDuration(j), phrases.get((i+1)%4)[j]); //durations.get(i)[j]
              float xyz = starts.get(i)[j] + ftime;
              System.out.println("Scheduled: " + i + " " + xyz + " " + m.getDuration(j));
              //System.out.println("Scheduled");
            }
            _offsets[i] = m.getRepeat();
            //_offsets[i+1] = m.getRepeat()+1;
          }
          m.drawChar(1);
        } else m.drawChar(2);
      }

      System.out.println();

      //step forward in time
      ftime += 1;

      for (int i = 0; i < 4; i++) {
        _offsets[i]--;
      }


      //play all the notes, for syncing
      for (int i = 0; i < out.length; i++) {
        out[i].resumeNotes();
      }



      //System.out.println("time is " + time);
      //f.drawGraph(ON);
  //  }else{
  //  for (int i = 0; i < out.length; i++) {

  //      MusicChar m = ((MusicChar)f.ms.get(i));
  //      if (_offsets[i] == 0) m.drawChar(1);
  //      else m.drawChar(2);
  //  }
  //}
  } else { for (int i = 0; i < out.length; i++) {

        MusicChar m = ((MusicChar)f.ms.get(i));
        m.drawChar(-1);
  }}
}




  //----------------------------MVMT ANIMATION----------------------------//

  //how quickly is something allowed to change?
  //depending on stopping conditions, subcolonal behaviors developing on extent of wave propagation

  //better to have the follower/leader behavior controlled by this class + have info stored in this class
  if (screenType == MVMT) {
    frameRate(240);
    screen.strokeWeight(2);

    //System.out.println(selected_chars.get(SC_OPT).size());
    if (SHOW_STATS) {

      for (int i = 0; i < selected_chars.get(SC_OPT).size(); i++) {
        System.out.println("hello");
        MvmtChar m = ((MvmtChar)fm.get(PD_OPT).ms.get((int)selected_chars.get(SC_OPT).get(i)));
        screen.fill(255);
        screen.text(m.getEndState(), m.getX()-40, m.getY() +55);
        screen.text(m.getStoreState(), m.getX()-40, m.getY()+65);
      }
    }

    if (RUN) {


      //default placement
      if (PD_OPT == DEF_OPT) {


        //implement way to select different leaders, change how following happens



        //1) implement leader/follower
        //2) make reset button clear all states, mvmtChar and musicChar should have their own methods

        //TIME scheduling vs. reaction time seem sensitive to each other... probably how mvmtChar is programmed/internal state-keeping
        if ((int)millis() > TIME && !PD_LOCK) { //periodically send state change requests to characters
          TIME += 4000;
          int r = (int)random(3);//for number of possible "states"
          System.out.println("r is " + r);
          screen.textFont(labelFont_l);
          //screen.text(millis()/1000, sHeight/5, sHeight/5-s_space);
          screen.text(r, sHeight/5, sHeight/7-s_space);

          for (int i = 0; i < 100; i++) {


            MvmtChar m = ((MvmtChar)fm.get(DEF_OPT).ms.get(i));
            m.setEndState(r); //when implementing user defined leader/follower, change r to be whatever leaders are

            //if (!((MvmtChar)m.getLeader()).isInMotion())
            m.schedule(100 + i*30); //wave propagation time + likelihood of chnage -> development of subcolonal behavior
            //following behavior implemented through delayed behavior
          }
        }

        //alignment
        for (int i = 0; i < 99; i++) {
          int E0 = ((MvmtChar)fm.get(DEF_OPT).ms.get(i)).getEndState();
          int E1 = ((MvmtChar)fm.get(DEF_OPT).ms.get(i+1)).getEndState();

          int S0 = ((MvmtChar)fm.get(DEF_OPT).ms.get(i)).getStoreState();
          int S1 = ((MvmtChar)fm.get(DEF_OPT).ms.get(i+1)).getStoreState();
          MvmtChar m = ((MvmtChar)fm.get(DEF_OPT).ms.get(i));
          //if (E1 != E0) { m.setEnd(E0); }
          if (S1 != S0) { 
            m.setStore(S0); 
            //System.out.println("Alignment Issue: " + i);
          }
        }
      } else { //user placed

        //find linear arrangement of order
        //go in that order and setEndState

        //body mvmt - below just testing sample, no rhyme or reason
        //for (int i = 0; i < fm.get(PLACE_OPT).ms.size(); i++) {

        //  MvmtChar m = (MvmtChar)fm.get(PLACE_OPT).ms.get(i);
        //  int n = fm.get(PD_OPT).getLeaders(i).get(0);
        //  int x = fm.get(PD_OPT).ms.get(n).getX();
        //  int y = fm.get(PD_OPT).ms.get(n).getY();
        //  if (m.getX() < x - 5) x = m.getX() + 5;
        //  else if (m.getX() > x - 5) x = m.getX() - 5;

        //  if (m.getY() < y - 5) y = m.getY() + 5;
        //  else if (m.getY() > y - 5) y = m.getY() - 5;

        //  m.setXY(x, y);
        //}

        //arm mvmt, currently not actually affected by leader/follower
        int r = (int)random(100); //implement way to select different leaders, change how following happens
        if (r<3) {

          for (int i = 0; i < fm.get(PLACE_OPT).ms.size(); i++) {
            MvmtChar m = (MvmtChar)fm.get(PLACE_OPT).ms.get(i);
            m.setEndState(r);
            m.schedule(100 + i*50); //millis()+i*200
            //IDEAS: 1) maybe something motion incorporation, 2) maybe something for quickly setting LF relationships


            //m.setXY(m.getX() + 1, m.getY() + 1);
          }
        }
      }

      screen.fill(250);
      if (frameCount % 5 == 0) {
        screen.textFont(labelFont_s);
        screen.text(millis(), sHeight/5, sHeight/7);
      }

      fm.get(PD_OPT).drawGraph(ON);
    } else fm.get(PD_OPT).drawGraph(OFF);
  }


  //if there's any request to clear selected characters
  if (CLEAR_SELECTED >= 0)
    clear_selected(MUSIC);


  //execute animation only if RUN toggle is online

  //stop drawing to the display screen
  //screen.scale(width/width_i, height/height_i);
  //cp5.setGraphics(this,0,0);
  //sWidth = screen.width;



  screen.endDraw();
  //width_i = width;
  //height_i = height;

  image(screen, LEFT_BORDER, TOP_BORDER);


  //g0.setHeight(height);//.setWidth(width - screen.width - LEFT_BORDER); 
  //g0.show();
  noStroke();
  fill(204);
  rect(screen.width+LEFT_BORDER, 0, width-screen.width, height);
  s_strict.update();
  cp5.draw();
}

//DECIDE if something needs to change
//if ((int)random(3) == 0) phrases.set(0, new String[] {"C5","D5","C5"}); //TEST


//MOVEMENT MODE ACTION
//need to distinguish between something following something because it's "next" and something updating next because leader changed

//void whichModes(int i) {
//  for (int j = 0; j < modes.get(i).length; j++)
//    if (random(10) > 3) {
//      modes.get(i)[j] = !modes.get(i)[j];
//    }
//}

boolean[] chooseActive(boolean[] activeModes) {
  boolean[] am = new boolean[activeModes.length];

  for (int i = 0; i < am.length; i++)
    am[i] = random(100) < 10? !activeModes[i]: activeModes[i];

  return am;
}


//interesting to check how parameter setting can affect largescale behavior
void updateMusic(int init) {
  CHANGE_DELAY = 8;
  FLIP = init;
  this.modes.set(FLIP, ((MusicChar)f.ms.get(FLIP)).getModel().flip()); //flip() updates model and modes.set updates this
  ((MusicChar)f.ms.get(FLIP)).getModel().updateModes(activeModes, modes.get(FLIP), (MusicChar)f.ms.get(FLIP));
  UPDATE = FLIP;

  int LEADER;
  for (int i = 0; i < VOICES-1; i++) {
    LEADER = UPDATE;
    UPDATE = f.ms.get(UPDATE).getFollowerID();
    //System.out.println(UPDATE);
    if (RELAX != 0) {
      //this.modes.set(UPDATE, ((MusicChar)f.ms.get(UPDATE)).getModel().flip()); //flip() updates model and modes.set updates this
      this.modes.set(UPDATE, ((MusicChar)f.ms.get(UPDATE)).getModel().follow(this.modes.get(UPDATE), this.modes.get(LEADER))); //introduces possibility of not flipping
    } else
      this.modes.set(UPDATE, modes.get(FLIP)); //strictly coheres to leader's behavior

    ((MusicChar)f.ms.get(UPDATE)).getModel().setModes(modes.get(UPDATE)); //so musicChar's internal data consistenn
    ((MusicChar)f.ms.get(UPDATE)).getModel().updateModes(activeModes, modes.get(UPDATE), (MusicChar)f.ms.get(UPDATE));
  }
}

//void updateMusic() {
//  //s = 0, a = 1, t = 2, b = 3;
//  // s -> b -> t -> a -> s

//  //grab current data

//  //decide if a voice is to drop out for a round


//  //decide if someone flips
//  if (FLIP!= UPDATE && UPDATE < 0) {
//    FLIP = (int)random(16); //SHOULDN'T HARDCODE
//    //System.out.println(FLIP + " random");
//    if (FLIP < VOICES) {
//      System.out.println(FLIP + " is changing");
//      //  //make the change to the chosen "flip" char
//      //  //flip will generate TRANSITION graph, method to set elements to certain values, which ones to set + values to set
//      //  //followers need to copy this transition graph, in their own time
//      //  //when turn, copy TYPE of change from leader, "TYPE" = which ones + what to flip to
//      //  //which ones to flip, what to flip to, current state (self-stored)


//      this.modes.set(FLIP, ((MusicChar)f.ms.get(FLIP)).getModel().flip()); //flip() updates model and modes.set updates thiss
//      ((MusicChar)f.ms.get(FLIP)).getModel().updateModes(activeModes, modes.get(FLIP), (MusicChar)f.ms.get(FLIP));

//      UPDATE = f.ms.get(FLIP).getFollowerID(); //pass on in next iteration to follower, this may be subject to change... should probably indicate delay instead
//      System.out.print(modes.get(0)[3]);
//      System.out.print(modes.get(1)[3]);
//      System.out.print(modes.get(2)[3]);
//      System.out.println(modes.get(3)[3]);
//    }
//  } else if (UPDATE >= 0 && FLIP != UPDATE) { //in process of cycling through/whichModes(UPDATE)
//    this.modes.set(UPDATE, modes.get(FLIP)); //for FlockingMain;
//    ((MusicChar)f.ms.get(UPDATE)).getModel().setModes(modes.get(UPDATE)); //so musicChar's internal data consistenn
//    ((MusicChar)f.ms.get(UPDATE)).getModel().updateModes(activeModes, modes.get(UPDATE), (MusicChar)f.ms.get(UPDATE));

//    System.out.print(modes.get(0)[3]);
//    System.out.print(modes.get(1)[3]);
//    System.out.print(modes.get(2)[3]);
//    System.out.println(modes.get(3)[3]);

//    //set follower's update boolean
//    UPDATE = f.ms.get(UPDATE).getFollowerID();
//    System.out.println("Update value: " + UPDATE);
//  } else if (FLIP == UPDATE) {
//    //set them back into unequal negative state, to be used again
//    FLIP = -1;
//    UPDATE = -2;
//    System.out.println("RESET");
//  }
//}


float sum(float[] arr) {
  float sum = 0;
  for (int i = 0; i < arr.length; i++) sum += arr[i];
  return sum;
}

//-----------------------------------------------------------------------------------------------------------
//----------------------------Control Event Section + Other Class--------------------------------------------

//deselect all chars that are currently selected
void deselect_all() {
  for (; numSelected > 0; numSelected--) {
    fm.get(PD_OPT).ms.get((int)selected_chars.get(SC_OPT).get(numSelected-1)).select();
    fm.get(PD_OPT).ms.get((int)selected_chars.get(SC_OPT).get(numSelected-1)).set_preselect(PRESELECT);
  }
  selected_chars.get(SC_OPT).clear();
  PRESELECT = true;
  lf_selected = 0;
  //wipe lf_control
  r_lf.deactivateAll();
  //r1.deactivateAll();


  //clear all buttons as well
  return;
}

//deselect the given chars
void deselect_all(int from) {
  int sc = selected_chars.get(SC_OPT).size();
  for (int i = from; i < sc; i++)
  {
    fm.get(PD_OPT).ms.get((int)selected_chars.get(SC_OPT).get(from)).select();
    fm.get(PD_OPT).ms.get((int)selected_chars.get(SC_OPT).get(from)).set_preselect(PRESELECT);
    selected_chars.get(SC_OPT).remove(from);
    numSelected--;
  }

  lf_selected = 0;
}


void addSelection(ArrayList<Integer> a) {
  int ln;

  for (int i = 0; i < a.size(); i++) {
    ln = a.get(i);
    fm.get(PD_OPT).ms.get(ln).select();
    fm.get(PD_OPT).ms.get(ln).set_preselect(PRESELECT);

    numSelected++;
    selected_chars.get(SC_OPT).add(ln);
  }
}

//Process different controlp5 events
void controlEvent(ControlEvent event) {
  println(event.getArrayValue());


  //MUSIC EVENTS

  if (event.isFrom(tempoKnob)) {
    //setRun(false);
    tempo = (int)event.getValue();
  }

  if (event.isFrom(activeModeSet)) {
    //System.out.println(event.getValue());
    //System.out.println(event.getId());
    for (int i = 0; i < event.getArrayValue().length; i++) {
      activeModes[i] = (int)event.getArrayValue()[i] == 1? true: false;
    }

    //System.out.println(i);
  }

  if (event.isFrom(s_strict)) {
    if ((int)event.getValue() < RELAX) STRICT_LESS = true;
    RELAX = (int)event.getValue();
  }


  //MOVEMENT EVENTS
  if (event.isFrom(state_input)) {
    System.out.println("Manual Input");
    PD_LOCK = true;
    r_lock.setValue(1);
    //screen.textFont(createFont("Georgia", 32));
    //screen.text(millis(), 300, 300);
    screen.text(state_input.getStringValue(), sHeight/5, sHeight/7-s_space);
    for (int i = 0; i < 100; i++) {

      MvmtChar m = ((MvmtChar)fm.get(DEF_OPT).ms.get(i));
      m.setEndState(Integer.valueOf(state_input.getStringValue()));

      //if (!((MvmtChar)m.getLeader()).isInMotion())
      m.schedule(i*50); //wave propagation time + likelihood of chnage -> development of subcolonal behavior
      //following behavior implemented through delayed behavior
    }
  }

  if (event.isFrom(show_label)) {
    System.out.println(event.getValue());
    LABEL_SHOW = (int)event.getValue() == 0? false:true;
  }

  if (event.isFrom(ldir_label)) {
    L_DIR = (int)event.getValue() == 0? false:true;
  }

  if (event.isFrom(stats_label)) {

    SHOW_STATS = (int)event.getValue() == 0? false:true;
  }

  if (event.isFrom(name_label)) {
    System.out.println(event.getStringValue());
    fm.get(PD_OPT).ms.get(char_selected).setText(event.getStringValue());
  }

  //The below are events that UNDO, thus PRESELECT = true;

  //deselect all characters
  //bug here reveals ID system needs to be consistent
  if (event.isFrom(ds)) {
    //deselect all chars, track which ones selected? maybe by index so they're easy to access? 
    //array.get(i).select(1); // set selection boolean to false
    deselect_all();
  }


  if (event.isFrom(clr)) {
    //deselect all chars, track which ones selected? maybe by index so they're easy to access? 
    //array.get(i).select(1); // set selection boolean to false
    //deselect_all();
    if (PD_OPT == PLACE_OPT) fm.get(PLACE_OPT).clearAll();
  }

  if (event.isFrom(exit_out)) {
    exit();
  }

  //if (event.isFrom(screenshot)) {
  //  String type = "";
  //  if (screenType == MUSIC) type = "music";
  //  else type = "mvt";
  //  saveFrame(type + " " + timestamp() +"_##.png");
  //}

  if (event.isFrom(rem)) {
    if (PD_OPT == PLACE_OPT && PD_LOCK) for (; numSelected > 0; numSelected--) { 
      fm.get(PLACE_OPT).ms.get((int)selected_chars.get(SC_OPT).get(numSelected-1)).select();
      for (int i = 0; i < fm.get(PLACE_OPT).ms.size(); i++)
        if (fm.get(PLACE_OPT).ms.get(i).getID() == (int)selected_chars.get(SC_OPT).get(numSelected-1))
          fm.get(PLACE_OPT).ms.remove(i);

      selected_chars.get(SC_OPT).remove(numSelected-1);
    }
  }

  //Below are events that ACTIVATE, thus PRESELECT = false;

  //r_pd determines whether movement chars are placed or drawn to a default/loaded shape/grid
  if (event.isFrom(r_pd)) {
    print("got an event from "+event.getName()+"\t");
    println("\t "+event.getValue());
    PD_OPT = (int)event.getValue();
    SC_OPT = PD_OPT;
    setRun(false);
  }


  //if r_pd == PLACE_OPT, whether to "lock" down the characters so selections can be made
  if (event.isFrom(r_lock)) {
    print("got an event from "+event.getName()+"\t");
    println("\t "+event.getValue());
    PD_LOCK = ((int)event.getValue() == 0) ? false: true;
  }

  //r1 determines...
  if (event.isFrom(r1)) {
    print("got an event from "+event.getName()+"\t");
    println("\t "+event.getValue());
    //myColorBackground = color(int(event.getGroup().getValue()*50),0,0);
    //see which is selected, if one has been previously selected, highlights leaders/followers

    int n = (int)event.getValue();
    //if current mode != -1 and n == -1, unhighlight_all();
    //else
    //highlighting can be used just for certain controls
    //go through highlighted list
    deselect_all();
    LAYER_MODE = n;


    if (n == -1) {
      //set current mode for Layer to -1
      return;
    }

    //set current mode to int corresponding to layer
    //highlight all chars corr to layer
    //then, if anything selected before layer ctrl turned off, added to layer
    for (Object c : layerlist.get(LAYER_MODE)) {
      fm.get(PD_OPT).ms.get((int)c).select();
      numSelected++;
      selected_chars.get(SC_OPT).add((int)c);
    }


    //highlight everything from that layer if that layer's list has size() > 0
    //switch (n){
    //  case 0: break;
    //  case 1: break;
    //  case 2: break;
    //  case 3: break;
    //  case 4: break;
  }

  //r_lf is leader/follower button
  if (event.isFrom(r_lf)) {
    print("got an event from "+event.getName()+"\t");
    println("\t "+event.getValue());
    float n = event.getValue();
    //myColorBackground = color(int(event.getGroup().getValue()*50),0,0);
    //see which is selected, if ONLY ONE has been previously selected, highlights leaders/followers
    //nothing/displays message if more than one selected... or can only select multiple if press another key concurrently
    //button for deselecting all selected

    //0 = Leader
    //1 = Follower

    //if user skipped from one option directly to another without deselecting first
    if (LF_OPT != -1 && (int)event.getValue()!= -1) {
      deselect_all(lf_selected);
    }

    LF_OPT = (int)event.getValue();

    int sc = selected_chars.get(SC_OPT).size();

    switch(LF_OPT) {
    case -1: 
      deselect_all(lf_selected);
      //if (LAYER_MODE == -1) {
      //  //System.out.println(selected_chars.get(SC_OPT).toString());
      //  //for (int i = lf_selected; i < sc; i++) {
      //  //  selected_chars.get(SC_OPT).remove(lf_selected);

      //}
      //deselect_all(); //maybe leave the leaders and let user deselect all if they want
      //we can keep track of PRESELECTED size

      //numSelected = PRESELECT_SIZE;
      //}

      SELECT_LOCK = false;
      PRESELECT = true;
      PRESELECT_SIZE = 0;
      break;
      //case 0:
      //  //char already selected is leader to all of next ones selected
      //  PRESELECT = false; //something's been activated
      //  lf_selected = selected_chars.get(SC_OPT).size();
      //  break;

      //case 1:
      //  //char_selected is follower
      //  lf_selected = selected_chars.get(SC_OPT).size();
      //  PRESELECT = false; //something's been activated
      //  break;

    case 0:
      //SELECT_LOCK = true;
      lf_selected = selected_chars.get(SC_OPT).size();
      PRESELECT = false; //something's been activated

      //if lf selected after Layer Ctrl selected, then should add everything selected into
      sc = selected_chars.get(SC_OPT).size();
      for (int i = 0; i < sc; i++) {
        ArrayList<Integer> a = fm.get(PD_OPT).getLeaders((int)selected_chars.get(SC_OPT).get(i));//(int)selected_chars.get(SC_OPT).get(i)); 
        addSelection(a);
      }
      //ArrayList<Integer> a = fm.get(PD_OPT).getLeaders(char_selected);//(int)selected_chars.get(SC_OPT).get(i)); 
      //addSelection(a);

      //ArrayList<Integer> a = fm.get(PD_OPT).getLeaders(char_selected);
      //addSelection(a);
      break;

    case 1:
      //SELECT_LOCK = true;
      lf_selected = selected_chars.get(SC_OPT).size();
      PRESELECT = false; //something's been activated

      //if lf selected after Layer Ctrl selected, then should add everything selected into
      //a = fm.get(PD_OPT).getFollowers(char_selected);//(int)selected_chars.get(SC_OPT).get(i)); 
      //addSelection(a);

      sc = selected_chars.get(SC_OPT).size();
      for (int i = 0; i < sc; i++) {
        ArrayList<Integer> a = fm.get(PD_OPT).getFollowers((int)selected_chars.get(SC_OPT).get(i)); 
        addSelection(a);
      }
      //a = fm.get(PD_OPT).getFollowers(char_selected);
      //addSelection(a);
      break;
    default:
    }

    if (!PRESELECT) PRESELECT_SIZE = selected_chars.size();

    //if (LF_OPT == -1) {
    //  deselect_all();
    //  SELECT_LOCK = false;
    //} else if ((int)n == 2) { //show leaders
    //  //if (SELECT_LOCK) // want to select everything except char_selected
    //  //{
    //  //  deselect_all();
    //  //  numSelected++;
    //  //  selected_chars.get(SC_OPT).add(char_selected);
    //  //}
    //  SELECT_LOCK = true;
    //  //int ln = fm.get(PD_OPT).getLeader(char_selected);
    //  //get all leaders and add them to numSelected/selected_chars
    //  ArrayList<Integer> a = fm.get(PD_OPT).getLeaders(char_selected);
    //  addSelection(a);
    //} else if ((int)n == 3) { //show followers
    //  SELECT_LOCK = true;

    //  ArrayList<Integer> a = fm.get(PD_OPT).getFollowers(char_selected);
    //  addSelection(a);
    //}
  }


  //update FlockGraph to new # of module characters
  if (event.isAssignableFrom(Textfield.class)) {
    println("controlEvent: accessing a string from controller '"
      +event.getName()+"': "
      +event.getStringValue()
      );
    numChars = int(event.getStringValue());
  }
}

//if you get an on the specific control panel slides + PAUSE (something is selected)
//then changes should be reflected in the selected object

//void module(int n)
//{
//  Map layerModeMap = cp5.get(ScrollableList.class, "module").getItem(n);
//  String layerMode = (layerModeMap.values().toArray()[0]).toString();
//  println (layerMode);
//}


//Key-commands
//ALT-mouseMove move controllers
//ALT-h show and hide controllers
//ALT-shift-s save controller setup in an properties-document
//ALT-shift-l load a controller setup from a properties-document
//Possibly add buttons to press instead of having user know 1/2/3/4?

//controller information saved, but maybe additionally save certain variables (like actors in each layer)
void keyPressed() {
  switch(key) {
    case('1'):
    println("currentValue: " +cp5.get(ScrollableList.class, "module").getValue());
    break;
    case('2'):
    cp5.saveProperties("hello.json");
    break;
    case('3'):
    cp5.loadProperties("hello.json");
  }
}

void keyReleased() {
  if (key == 's' || key == 'S') {
    String type = "";
    if (screenType == MUSIC) type = "music";
    else type = "mvt";
    saveFrame(type + " " + timestamp() +"_##.png");
  }
}


void module(int n) {
  /* request the selected item based on index n */

  //println(n, cp5.get(ScrollableList.class, "module").getItem(n).get("name"));
  Object o = cp5.get(ScrollableList.class, "module").getItem(n).get("name");
  //println("Module is " + o);
  if (screenType == MUSIC && RUN || o == "music") { //pause player whenever music screen is invoked and run is on
    for (int i = 0; i < out.length; i++)
      out[i].pauseNotes();
    setRun(false);
    //set RUN button to off position
  } //pause everything
  if ( o == "music") {
    screenType = MUSIC;
    opacity = 250;
    //f.getOrder(false);
    //showed the lines... or a least the notes somehow... might be too intensive though
    //screenColor = color(152, 210, 218);
    //display music module menu
    //
  } else {
    screenType = MVMT;
    if (PD_OPT == PLACE_OPT) fm.get(PLACE_OPT).getOrder(false);
    //display mvmt module menu
    //
  }
  //screenColor = color(223, 210, 218);
  /* here an item is stored as a Map  with the following key-value pairs:
   * name, the given name of the item
   * text, the given text of the item by default the same as name
   //* value, the given value of the item, can be changed by using .getItem(n).put("value", "abc"); a value here is of type Object therefore can be anything
   //* color, the given color of the item, how to change, see below
   //* view, a customizable view, is of type CDrawable 
   //*/

  //CColor c = new CColor();
  //c.setBackground(color(255, 0, 0));
  //cp5.get(ScrollableList.class, "module").getItem(n).put("color", c);
}

void runToggle(boolean theFlag) {
  if (theFlag==true) {
    RUN = true; 
    println("True");
  } else {
    RUN = false;
    //if (screenType == "music"){for (int i = 0; i < out.length; i++) out[i].pauseNotes();}
    //maybe add color indicator that simulator isn't running... something greyer?
    //tell colors to STAY OFF
  }
  //pause all operations
  //out[i].pauseNotes();
}

void setRun(boolean flag) {
  if (flag) {
    RUN = true; 
    cp5.get(Toggle.class, "runToggle").setValue(1);
  } else {
    RUN = false; 
    cp5.get(Toggle.class, "runToggle").setValue(0);
  }
}

boolean inside(ModuleChar m) {
  return (mouseX >=m.getX() && mouseX <=m.getX()+m.getH() && mouseY >=m.getY() && mouseY <=m.getY()+m.getH());
}

int inside(ArrayList<ModuleChar> ms) {
  int n = -1;
  for (int i = 0; i < ms.size(); i++) {
    if (inside(ms.get(i))) return i;
  }
  return n;
}

void mouseDragged() {
  if (mouseX > screen.width) return;
  fill(120, 150, 150);
  ellipse(mouseX, mouseY, 5, 5 );
  dragged = true;
  //int i = inside(f.ms);
  //if (i >= 0)
  // f.ms.get(i).select();
}

void mousePressed() {
  if (mouseX > screen.width) return;
  if (screenType == MUSIC) {
    int i = inside(f.ms);
    if (i >= 0) {
      setRun(false);
      f.ms.get(i).select();
      selected_chars.get(MUSIC).add(i);
      char_selected = i;
    }
  }
}

void mouseReleased() {
  if (mouseX > screen.width) return;
  int i = inside(f.ms);
  if (i >= 0 && dragged) {
    f.ms.get(i).select(); //need to account for movement later
    selected_chars.get(screenType).add(i);
    //make current i follow previous char_selected
    f.addEdge(f.ms.get(i).getID(), f.ms.get(char_selected).getID()); //1st follows 2nd
  }
  if (dragged) {
    CLEAR_SELECTED = screenType;
  }
  dragged = false;
}

int clear_selected(int screenType) {
  int n = 0;

  if (CLEAR_SELECTED >= 0) {

    for (int i = 0; i < selected_chars.get(screenType).size(); i++) {
      int s = (int) selected_chars.get(screenType).get(i);
      if (screenType == MUSIC) f.ms.get(s).select(false);
      if (screenType == MVMT);
    }

    selected_chars.get(screenType).clear();
  }

  CLEAR_SELECTED = -1;
  return n;
}


void mouseClicked() {
  //locate and choose appropriate circle... potential area of speed-up
  //if (screenType == MUSIC) {
  //  for (int i = 0; i < f.ms.size(); i++) {
  //    ModuleChar m = f.ms.get(i);
  //    if (inside(m)) {
  //       m.select();
  //      selected_chars.get(MUSIC).add(i);
  //    }
  //  }
  //}
  if (mouseX > screen.width) return;
  if (screenType == MVMT) {

    //if (SELECT_LOCK) {
    //  //fm.get(PD_OPT).ms.get(char_selected).set_preselect(PRESELECT);
    //  PRESELECT = false;
    //  return;
    //}

    //have to take mode into account

    if (PD_OPT == PLACE_OPT && !PD_LOCK) {
      fm.get(PLACE_OPT).addChar(new MvmtChar(mouseX-30, mouseY-30));
      int n = fm.get(PLACE_OPT).ms.size();
      if (n > 1) {
        fm.get(PLACE_OPT).addLeader(n-2, n-1); //possibility of adding by name or ID #?
        fm.get(PLACE_OPT).addFollower(n-2, n-1);
      } else {
        fm.get(PLACE_OPT).addLeader(n-1, n-1); //possibility of adding by name or ID #?
        fm.get(PLACE_OPT).addFollower(n-1, n-1);
      }
    }

    if (PD_OPT == DEF_OPT || (PD_OPT == PLACE_OPT && PD_LOCK == true)) {
      for (int i = 0; i < fm.get(PD_OPT).ms.size(); i++) {

        ModuleChar m = fm.get(PD_OPT).ms.get(i);
        if (mouseX < m.getX()+m.getH() && mouseX > m.getX()
          && mouseY < m.getY() + m.getH() && mouseY > m.getY()) {
          m.select();

          //set leaders/followers if LFButton is not -1 (off)
          if (LF_OPT != -1) {
            //if (LF_OPT == LEAD_OPT || LF_OPT == 3) {
            //  System.out.println(lf_selected);
            //  for (int j = 0; j < lf_selected; j++) {
            //    int sc = (int)(selected_chars.get(SC_OPT).get(j));
            //    if (!fm.get(PD_OPT).getFollowers(sc).contains((Integer)i)) {
            //      fm.get(PD_OPT).addFollower(sc, i);
            //      fm.get(PD_OPT).addLeader(sc, i);
            //    }
            //  }
            //  System.out.println("Followers doesn't contains");
            //} else 
            if (LF_OPT == 1) { //show followers
              for (int j = 0; j < lf_selected; j++) {
                int sc = (int)(selected_chars.get(SC_OPT).get(j));
                if (!fm.get(PD_OPT).getFollowers(sc).contains((Integer)i)) {

                  if (fm.get(PD_OPT).getLeaders(sc).contains((Integer)i))
                    fm.get(PD_OPT).getLeaders(sc).remove((Integer)i);

                  fm.get(PD_OPT).addFollower(sc, i);
                  fm.get(PD_OPT).addLeader(sc, i);
                } else {
                  fm.get(PD_OPT).getFollowers(sc).remove((Integer)i); //this will remove the character at the index
                  fm.get(PD_OPT).getLeaders(i).remove((Integer)sc);
                }
              }
            }

            //if (LF_OPT == FOLLOW_OPT || LF_OPT == 2) {
            //  for (int j = 0; j < lf_selected; j++) {
            //    int sc = (int)(selected_chars.get(SC_OPT).get(j));
            //    if (!fm.get(PD_OPT).getLeaders(sc).contains((Integer)i)) {
            //      fm.get(PD_OPT).addLeader(i, sc);
            //      fm.get(PD_OPT).addFollower(i, sc);
            //    }
            //  }
            //} else 
            if (LF_OPT == 0) {
              for (int j = 0; j < lf_selected; j++) {
                int sc = (int)(selected_chars.get(SC_OPT).get(j));
                if (!fm.get(PD_OPT).getLeaders(sc).contains((Integer)i)) {

                  if (fm.get(PD_OPT).getFollowers(sc).contains((Integer)i))
                    fm.get(PD_OPT).getFollowers(sc).remove((Integer)i);

                  fm.get(PD_OPT).addLeader(i, sc);
                  fm.get(PD_OPT).addFollower(i, sc);
                } else {
                  fm.get(PD_OPT).getFollowers(i).remove((Integer)sc);
                  fm.get(PD_OPT).getLeaders(sc).remove((Integer)i);
                }
              }
            }
          }


          System.out.println("chose");
          if (m.isSelected()) {

            setRun(false);
            numSelected++;
            selected_chars.get(SC_OPT).add(m.getID());
            char_selected = i;

            //no particular g2 module parameters have been chosen
            m.set_preselect(PRESELECT);

            //group 2 command panel shouldn't always show...? or modify values for all.... but if current values display then deselect all except last... when trying to modify
            //need individual/multiple select options
            r_lf.show();
            rlf_label.show();
            name_label.show();
            labeling.show();



            //if only one selected, then can choose followers/leaders
            //parameters can group set? but certain ones display current/allow modification
            //choose multiple and choose multiple leaders/followers?
            //

            //if some layer is active, add to that layer UNLESS follower/leader is also active
            if (LAYER_MODE != -1 && !(LF_OPT == 0 || LF_OPT == 1)) {
              layerlist.get(LAYER_MODE).add(m.getID());
            }
            //don't deselect others so we can set many mods to same values
          } else { 
            numSelected--; 
            selected_chars.get(SC_OPT).remove((Integer)m.getID()); //(Integer)m.getID()
            System.out.println(selected_chars.get(SC_OPT).toString());

            //remove from the given LAYER
            int c = 0;
            if (LAYER_MODE != -1) {
              for (Object p : layerlist.get(LAYER_MODE)) {
                if ((int)p == i) { 
                  layerlist.get(LAYER_MODE).remove(c); 
                  break;
                } //if found ID in this layerlist, remove it
                c++;
              }
            }
            if (numSelected == 0) {
              r_lf.deactivateAll();
              r_lf.hide();
              rlf_label.hide();
              name_label.hide();
            }
            //g2.close();
            //g2.enableCollapse();
            //g2.hide();
            //instead have features on the modify module selectively appear/disappear depending on number of chars selected
          }
        }

        //System.out.println(numSelected);
        //if any mod character is selected, pause the animation

        //when mod character isSelected, subsequent changes using the UI should apply to those only
        //if changes are made while animation happening, pause animation
      }
    }
  }
}

// timestamp
String timestamp() {
  Calendar now = Calendar.getInstance();
  return String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", now);
}

void Screenshot() {
  String type = "";
  if (screenType == MUSIC) type = "music";
  else type = "mvt";
  saveFrame(type + " " + timestamp() +"_##.png");
  cp5.saveProperties(type + " " + timestamp() + ".json");
}

//void RELAXNESS(){
//  if (s_strict.getValue() < RELAX) STRICT_LESS = true;
//    RELAX = (int)s_strict.getValue();
//}
