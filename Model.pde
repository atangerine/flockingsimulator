//import beads.*;
//import org.jaudiolibs.beads.*;
import java.util.Map;


//Model changes wavelets and movement modules according to Trueman/Leonard's waveguide model

class Model {
}
//KNOBS: strictness, tempo, dynamics, register, ornamentation....
//strict <-> relaxed, resistive <-> changeable, repulsed <-> attracted

//Buttons: interrupt, leave, re-enter, conspire, reorder, jump-cut
//could be grid created based on the number of "agents" drop-down

//model class for both... extended... constraints creating waveguide

//wavelet 
//rising/falling

//open/closed

//soaring/beating (tied notes)

//big/small (interval)

//rhythm, articulation, dynamics --> expressive features

//jump-cuts


//maybe use lights to signal wave propagation

class MusicModel extends Model {

  String[] phrase;
  HashMap<String, Integer> noteMap;
  String[] numMap;
  String[] scale = {"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"};

  //stack of fourths spanning ~ entire range of saxophone quartet
  String[] hingeTones = {"G#6", "D#6", "A#5", "F5", "C5", "G4", "D4", "A3", "E3", "B2", "F#2", "C#2"};
  int beats;

  //strict wavelet features
  int NUM_FEATURES = 4;
  int RISE_FALL = 0;
  int OPEN_CLOSED = 1;
  int SOAR_BEAT = 2;
  int BIG_SMALL = 3;

  boolean RISE = true;
  boolean OPENED = true;
  boolean SOAR = true;
  boolean BIG = true;

  boolean FALL = false;
  boolean CLOSED = false;
  boolean BEAT = false;
  boolean SMALL = false;

  int CHANGE_LOC = 1;
  int BPM = 4; //beats per measure, later implement ability to change through GUI

  MusicChar m;
  boolean[] modes;
  boolean[] active_modes;




  //Parameters for each wavelet feature
  //Rise fall... where in wavelet does reversal happen/for how many notes
  //open_closed... where in wavelet, for how many notes?
  //soar_beat = how long... is each wavelet defined to be? how do we "sync" them, do we want to sync them
  //big_small = size of jump, M2/M7 or something else


  //current phrase

  //characteristics of current phrase

  //characteristics of leader's phrase

  MusicModel(MusicChar m) {

    this.m = m;
    //this.active_modes = active_modes;
    this.modes = new boolean[NUM_FEATURES];
    //modes[RISE_FALL] = 0; //irrelevant given features but placeholder in case
    //modes[OPEN_CLOSED] = 0;
    //modes[SOAR_BEAT] = 0;
    //modes[BIG_SMALL] = 0;
    //instantiate such that tied to MusicChar, call directly to modify


    //map numbers to frequencies?


    //modes = different mode behavior to activate for this model

    //map String notes to numbers and numbers to String notes
    noteMap = new HashMap<String, Integer>(88);

    numMap = new String[89];


    noteMap.put("NA", 0);
    noteMap.put("A0", 1);
    noteMap.put("A#0", 2);
    noteMap.put("B0", 3);

    numMap[0] = "NA";
    numMap[1] = "A0";
    numMap[2] = "A#0";
    numMap[3] = "B0";

    for (int i = 0; i<85; i++) {
      String note = scale[i%scale.length];
      int oct = i/scale.length + 1;
      noteMap.put(note + oct, i+4);
      numMap[i+4] = note + oct;
    }

    //for (int i = 1; i < 10; i++) {
    //  System.out.println(noteMap.get(numMap[i]));
    //}

    //for (int i = 1; i < 10; i++) {
    //  System.out.println(numMap[i]);
    //}


    //map elements of modes to a behavior

    //set those behaviors on for this particular instance of musicModel

    //call methods accordingly to build combo... 
    //or generate all possible combos and store...
  }

  //potentially a visualization schema, four rows of blocks representing four voices, col # = number of modes, each lit/not
  //black = off, same color but dark vs. light for voices
  //modify
  boolean[] flip() {
    for (int i = 0; i < modes.length; i++) {
      if (activeModes[i])
        this.modes[i] = !modes[i];
    }
    return this.modes;
  }
  
  
  //do we maintain unflipped?
  boolean[] follow(boolean[] modes, boolean[] ref){
    
    for (int i = 0; i < modes.length; i++) {
      if (random(30) < RELAX) //maintain current state
        this.modes[i] = modes[i];
      else
    this.modes[i] = ref[i];
    }
    return this.modes;
    
  }

  //boolean[] flip(float p) {
  //  for (int i = 0; i < modes.length; i++) {
  //    if (activeModes[i])
  //      if (random(100) > (int)p*100)
  //        this.modes[i] = !modes[i];
  //  }
  //  return this.modes;
  //}

  //switch up which activeModes there are
  //void whichModes(){
  //  for (int i = 0; i < modes.length; i++){
  //    if (random(10) > 3){
  //      activeModes[i] = !activeModes[i];
  //    }
  //  }
  //}



  //combine the above
  void updateConditions() {
    //which modes to flip
    //following order
    //whether to drop out

    //change hinge pitches

    //change later
    //change following time
    //expressive - dynamcs, ornaments, so on
  }

  String[] newHinge(boolean RISE_FALL, boolean BIG_SMALL, String name) {
    int sign = RISE_FALL == RISE? 1: -1;
    int step = BIG_SMALL == BIG? 11: 2;
    int x0 = 0;
    int x1 = hingeTones.length;

    switch(name) {
    case "s":
      x0 = 3;
      x1 = 7;
      break;
    case "a":
      x0 = 4;
      x1 = 8;
      break;
    case "t":
      x0 = 6;
      x1 = 10;
      break;
    case "b":
      x0 = 7;
      x1 = 11;
      break;
    default:
      break;
    }


    String a = hingeTones[(int)random(x0, x1)];
    int i = noteMap.get(a) + sign*step;
    while (i < 0 || i > 88) {
      a = hingeTones[(int)random(x0, x1)];
      i = noteMap.get(a) + sign*step;
    }

    String b = numMap[i];
    String[] str = {a, b};
    return str;
  }


  void updateModes(boolean[] active_modes, boolean[] modes, MusicChar mc) {
    //fall through and modify as necessary

    String a = mc.getPhrase()[0];
    String b = mc.getPhrase()[1];


    //open closed
    if (active_modes[OPEN_CLOSED]) { //set duration back...to... need original/working... another one is working with the length
      //open - take off third note of wavelet


      if (modes[OPEN_CLOSED] == OPENED) { //set duration to 0?
        mc.setLength(2); //eventually make more dynamic
        System.out.println("OPENED" + OPENED);
      }

      //closed - add back third note of wavelet
      if (modes[OPEN_CLOSED] == CLOSED) {
        mc.setLength(3);
        System.out.println("CLOSED");
      }
    }


    //soaring beating
    if (active_modes[SOAR_BEAT]) {
      //set third note beat to beats - duration of first couple of notes
      //set duration of third beat to something different, update REPEAT value;
      //storing local copy in FlockingMain for ease of access, but also keep local value, small storage tradeoff

      float[] durations = mc.getDurations();//change to BPM - first two notes' durations
      if (modes[SOAR_BEAT] == SOAR) {
        System.out.println("SOAR " + active_modes[RISE_FALL]);
        //change repeat value?
        mc.setRepeat(4); //later change to BPM, repeat every 4th beat
        if (mc.getLength() < 3) mc.setDuration(1, 3.75);//BPM - durations[0]); //use getLength to account for open/closed
        else mc.setDuration(mc.getLength() - 1, 3.5);//BPM - durations[0] - durations[1]);
      }

      if (modes[SOAR_BEAT] == BEAT) {
        System.out.println("BEAT");
        mc.setRepeat(4); //repeat every beat
        if (mc.getLength() < 3) mc.setDuration(1, 0.25);//BPM - durations[0]); //use getLength to account for open/closed
        else { mc.setDuration(1, 0.5); mc.setDuration(2, 0.5);}//BPM - durations[0] - durations[1]); //change to duration of first note FOR NOW mc.getLength() - 1
      }


      //each character has a model class that describes whether on/off + which mode
    }

    //big small - already flipped mode, now just update
    if (active_modes[BIG_SMALL]) {
      //big - minor 7th above/below -- note num +/- 11
      int sign = noteMap.get(a) > noteMap.get(b)? 1:-1;
      int step = modes[BIG_SMALL] == BIG? 11:2;
      b = numMap[noteMap.get(a)+step*sign];
      //System.out.println("NEW SECOND NOTE " + b);
      m.setNote(b, CHANGE_LOC);
    }


    //rising falling - change interval
    if (active_modes[RISE_FALL]) {
      int sign = modes[RISE_FALL] == RISE? 1: -1;
      String result = flipInterval(a, b, sign);
      //modes should have been updated in flipInterval
      mc.setNote(result, CHANGE_LOC);//think about what 1 really means
      //System.out.println(result);

      //add to loc
    }

    //change hinge tone
    if (random(100) > 70) {
      //get new hingeTone
      String[] str = newHinge(modes[RISE_FALL], modes[BIG_SMALL], mc.name());
      a = str[0];
      b = str[1];
      mc.setPhrase(0, a);
      mc.setPhrase(1, b);
      if (mc.getPhrase().length > 2) {
        mc.setPhrase(2, a);
      }
    }

    int[] nums = {noteMap.get(a), noteMap.get(b)};
    //D4 = 42
    mc.setPhraseNums(nums);
  }


  void setActiveModes(boolean o, int FEATURE)
  {
    active_modes[FEATURE] = o;
  }
  void setActiveModes(boolean[] os) {
    active_modes = os;
  }

  boolean[] getActiveModes() {
    return active_modes;
  }

  void setModes(boolean o, int FEATURE) {
    modes[FEATURE] = o;
  }

  //only set the mode if the mode is to be set
  void setModes(boolean[] os) {
    for (int i = 0; i < os.length; i++) {
      if (activeModes[i])
        this.modes[i] = os[i];
    }
  }

  //void setModes(boolean[] os, float p) {
  //  for (int i = 0; i < os.length; i++) {
  //    if (random(100) < (int)p*100)
  //      if (activeModes[i])
  //        this.modes[i] = os[i];
  //  }
  //}

  boolean[] getModes() {
    return modes;
  }


  //find int i,j mapped to a and b, take difference... newNote = i - diff;
  String flipInterval(String a, String b, int sign) {

    //should technically check that a and b is within range...

    //convert input with flats
    if (a.contains("b")) {
      a = convertFlats(a);
    }
    if (b.contains("b")) {
      b = convertFlats(b);
    }

    //go up/down
    int n1 = noteMap.get(a);
    int n2 = noteMap.get(b);

    System.out.println(a + " " + b + " " + n1 + " " + n2);

    int interval = abs(n1 - n2); //positive if note n1 is higher than n2
    //if (interval < 0) modes[RISE_FALL] = FALL;
    //else modes[RISE_FALL] = RISE;

    return numMap[n1 + interval*sign];
  }

  String convertFlats(String n) {
    String note = n.charAt(0) + "";
    if (n.length() > 2)
      note += n.charAt(2);
    else
      note += "4";

    return numMap[noteMap.get(note)-1];
  }

  int getInterval(String a, String b) {
    int n1 = noteMap.get(a);
    int n2 = noteMap.get(b);

    return n1 - n2;
  }

  //change to new module relative to leader
  void update(int id) {
    //get leader, get behavior

    //get rule

    //update according to rule
  }
}


class mvmtModel extends Model {
}
