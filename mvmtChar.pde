class MvmtChar extends ModuleChar {

  //STATES
  int D = 0; //arms down
  int H = 1; //horizontally held
  int S = 2; //UP in the air, slanted

  //int S = 3; //slanted in diagonal

  float shift = 0;
  float shift_amt = PI/64; //how fast to move
  float delay = 1000;
  float nextTime;
  float store_nextTime;
  boolean UPDATE = false;
  boolean LOCK = false;

  //POSITIONS
  int inc = 15;
  int c = 2;
  //float[][] posX = {{-inc, inc}, {-inc, inc}, {-c*inc, c*inc}, {inc - c*inc/sqrt(2), inc + c*inc/sqrt(2)}};//each mvmtChar defined by four points/two arms
  //float[][] posY = {{inc, inc}, {-inc, -inc}, {0, 0}, {inc - c*inc/sqrt(2), inc - c*inc/sqrt(2)}};
  //float[][] angles = {{3*PI/4,PI/4},{-PI, 0},{3*PI/2, 3*PI/2}};
  float[] angles = {0, PI/2, 3*PI/4};



  //int c = 1;
  int x0, x1, y0, y1;
  float ROT;
  int END;
  int STORE_END;
  int STATE;
  boolean MET;
  boolean IN_MOTION; // maybe change to 0 = unset, 1 = in_motion, 2 = at end?
  boolean WAITING; //when done and waiting for new

  int TYPE = 1;

  //Rule set... maybe each initialized with different set of rules?

  MvmtChar(int x, int y) {
    super(x, y);
    x0 = x-inc;
    x1 = x+inc;
    y0 = y;
    y1 = y;
    ROT = 0;

    STORE_END = -1;
    END = D; //arms down neutral phase
    STATE = D; //don't move, current state = end state
    nextTime = 0;
    store_nextTime = -1;
    
    WAITING = true;

    IN_MOTION = false;

    //for (int i = 0; i < posX.length; i++) {
    //  posX[i][0] += x0;
    //  posX[i][1] += x1;
    //  posY[i][0] += y0;
    //  posY[i][1] += y1;
    //}
  }

  void setXY(int x, int y) {
    this.x = x;
    this.y = y;
    x0 = x-inc;
    x1 = x+inc;
    y0 = y;
    y1 = y;
  }


  int getType() {
    return TYPE;
  }
  //initialized?
  //index, speed, maxSpinAngle, randomNoisePercent, location, facing, direction, color

  //Formation: random, preset values, custom?

  //Speed
  //Max Spin Speed
  //Noise
  //X, Y coordinates of location
  //Facing direction (individual modification)

  //Behavioral rules

  //add obstacles... for later


  //place addition "lines" centered at location of ModuleChar
  //  modify these lines as necessary

  //call update at each time step\
  void schedule(float time) {

    if (WAITING || STORE_END == -1 && abs(shift - angles[END]) > abs(shift_amt)) 
      nextTime = millis() + time;
    else
      store_nextTime = millis() + time;
      
    if (WAITING) WAITING = false;
    //if (!LOCK) {
    //  nextTime = time;
    //  LOCK = true;
    //}
  }


  void update() { //instead of this.end

    //int intermediate = -1;
    //for (int i = 0; i < angles.length; i++) {
    //  if (angles[i] != END && angles[i] != STATE) intermediate = i;
    //}

    WAITING = false;
    //detect shift in angle
    if ((angles[END] > shift && shift_amt < 0) || (angles[END] < shift && shift_amt > 0)) shift_amt *= -1;

    //if ((END < STATE && shift < angles[END] && shift_amt < 0) || (END > STATE && shift > angles[END] && shift_amt > 0)) shift = angles[END];

    //move a bit towards END
    if (abs(shift-angles[END]) > abs(shift_amt)) {
      shift+= shift_amt;
      
      //if (id == 0)
      //  System.out.println("END: "  + END + " Shift: " + shift + " Shift amt: " + shift_amt + " ");

      //END reached
      if (abs(shift-angles[END]) < abs(shift_amt) || (store_nextTime != -1 && store_nextTime < millis())){
        
        if (id == 0)
        System.out.println("END changing event");
        
        if (abs(shift-angles[END]) < abs(shift_amt)) {
          STATE = END; 
          shift = angles[END];
          IN_MOTION = false;
        }
        else{
          if (id == 0) System.out.println("Passed next time");
        }
        
        
        if (STORE_END != -1) { 
          END = STORE_END;
          nextTime = store_nextTime;
          if (id == 0) System.out.println(END + ": Updated END and store_nextTime" + "to " + END);
        }
        else {WAITING = true; if (id == 0) System.out.println("WAITING FOR NEXT STATE...");}
        

        //if (store_nextTime != -1) {
        // nextTime = store_nextTime;
        //}



        STORE_END = -1;
        store_nextTime = -1;

        UPDATE = false;
        LOCK = false;
        
      }

      //if (id == 5) System.out.println(shift + " " + END + " " + STATE);
    }


    //correct for floating point errors
    if (shift < angles[0]) shift = angles[0];
    if (shift > angles[angles.length-1]) shift = angles[angles.length-1];
  }

  void setRotation(float rot) {
    ROT = rot;
  }

  //call this method to change movement, need to control flocking behavior elsewhere
  void setEndState(int end) {

    //may not be IN_MOTION yet but already updated again
    if (nextTime == 0 || WAITING) { //beginning or finished going through everything
      END = end;
      //WAITING set to false in schedule method
      if (id == 90) System.out.println("Just scheduled + " + id + ":  " + STATE + " "  + END + " " + STORE_END);
      return;
    }
    
    

    if (abs(shift - angles[END]) > abs(shift_amt) || STORE_END == -1 && IN_MOTION){ //currently moving
      if (id == 0) System.out.println("Store End Update " + STORE_END + " " + shift_amt);
      STORE_END = end;
    }
    else {
      this.END = end;
      if (id == 0) System.out.println("Updating end");
    }
    UPDATE = true;
    LOCK = false;

    if (id == 0) System.out.println("Motion: " + IN_MOTION);
    if (id == 0) System.out.println(id + ":  " + STATE + " "  + END + " " + STORE_END);
    if (id == 50) System.out.println(id + ":  " + STATE + " " + END + " " + STORE_END);
    if (id == 90) System.out.println(id + ":  " + STATE + " "  + END + " " + STORE_END);
  }

  int getEndState() {
    return END;
  }

  int getStoreState(){
    return STORE_END;
  }
  
  void setEnd(int end){
    END = end;
  }
  
  void setStore(int store){
    STORE_END = store;
  }
  
  void setState(int state) {
    this.STATE = state;
  }
  

  int getState() {
    return STATE;
  }

  boolean isInMotion() {
    return IN_MOTION;
  }


  boolean drawChar(int on, boolean motion) {

    //updating but not reacting
    


    if (END != -1 && 
    ((on == 0 && nextTime < millis()))){// || ( on == 0 && abs(shift - angles[END]) > abs(shift_amt) && IN_MOTION))) {

      //System.out.println(IN_MOTION);
      if ((abs(shift-angles[END]) > abs(shift_amt))) {
        IN_MOTION = true;
        update();
      } 
      else IN_MOTION = false;
    }
    //LOCK = false; //received the next cue, so okay to change
    //} else if ((abs(shift-angles[END]) > abs(shift_amt))) { 
    //  if (STORE_END != -1) END = STORE_END; 
    //  nextTime = store_nextTime;
    //  IN_MOTION = false;
    //  STORE_END = -1;
    //  if (id == 50) System.out.println("here");
    //}
    //if (STATE != PREV_END) update();


    //Indicator of whether selected or not
    super.drawChar(on, motion);
    screen.line(x0, y0, x0+c*inc*sin(-shift), y0+c*inc*cos(-shift));
    screen.ellipse(x, y, inc, inc);
    screen.line(x1, y1, x1+c*inc*sin(shift), y1+c*inc*cos(shift));
    screen.fill(255);
    screen.text(nextTime, x-40, y+45);
    //screen.line(x0, y0, x0, y0+20);
    //screen.line(x1, y1, x1, y1+20);
    //shift+= 0.2;


    //screen.line();
    //line needs to transition from one form to another
    return IN_MOTION;
  }


  //void animateChar() {
  //  screen.line(x0, y0, x0+c*inc*sin(-shift), y0+c*inc*cos(-shift));
  //  screen.ellipse(x-inc/2, y-inc/2, inc, inc);
  //  screen.line(x1, y1, x1+c*inc*sin(shift), y1+c*inc*cos(shift));
  //  shift+= shift_amt;
  //}
}
//class MvmtChar extends ModuleChar{
//  //depiction of movement
//  //

//place addition "lines" centered at location of ModuleChar
//modify these lines as necessary


//  //left arm
//  void setLeft(){
//    //get area to the left of module's center
//  };

//  //right arm
//  void setRight(){
//    //get area to right of module's center
//  };


//  //
//}

////left arm
//void setLeft(int state) {
//  //get area to the left of module's center
//  //switch(state) {
//  //case U:
//  //  float[] posY[0] = ; 
//  //  float[] posY[3] =; 
//  //  break;
//  //case D:
//  //  ; 
//  //  break;
//  //case DIAG:
//  //  ; 
//  //  break;
//  //}
//}

////right arm
//void setRight(int state) {
//  //get area to right of module's center
//  //      switch(state){
//  //    case U:; break;
//  //    case D:; break;
//  //    case DIAG:; break;
//  //}
//}
