

class MusicChar extends ModuleChar {

  //audio files vs generating sound within processing...
  //latter allows more flexibility... delays?

  //setting tempo, 

  //musicChar contains metadata about the character's behavior while musicPlayer/model? takes care of the music synthesis/processing

  //NOTE STATES
  int PHASE = -1;
  int ANTIPHASE = -2;

  //The model which helps construct and play features
  MusicModel mod;

  //models notes and beats to be passed to Minim playNote
  String[] phrase;
  int[] phraseNums;
  int[] beats;
  float[] durations;
  float[] starts;
  int[] states; //what each mode's state is

  //instead of making change directly to class, make it to the queue then load the queue options in when it's time, which can be monitored by a "lag" parameter
  float[] durationsQ;
  float[] startsQ;
  float[] lengthQ;
  String[] phraseQ;
  boolean[] modesQ;
  boolean[] activeModesQ;


  int pLength = 0;
  float soar_val;
  float beat_val;
  int repeat;

  //boolean[] activeModes;
  //boolean[] modes;
  //What type of Instrument/does it matter?
  int RISE_FALL = 0;
  int OPEN_CLOSED = 1;
  int SOAR_BEAT = 2;
  int BIG_SMALL = 3;

  //color palette
  color[] set1 = {color(126, 20, 23), color(75, 22, 81), color(21, 21, 72), color(0, 98, 72)};
  color[] set2 = {color(245, 154, 167), color(166, 118, 180), color(132, 167, 216), color(171, 215, 165)};

  boolean update = false;
  //PITCH STATES

  void update(boolean state) {
    update = state;
  }

  boolean update() {
    return update;
  }

  int locX;
  int locY;
  String name;
  int TYPE = 0;

  MusicChar(int x, int y, String name) {
    super(x, y, name);
    locX = x;
    locY = y;
    this.name = name;

    activeModes = new boolean[4]; //can store additional control features in activeModes, highest level control, whether something is to exist or not
    //modes = new boolean[4];
    //activeModes[RISE_FALL] = false;
    //activeModes[OPEN_CLOSED] = false;
    //activeModes[SOAR_BEAT] = false;
    //activeModes[BIG_SMALL] = true;

    mod = new MusicModel(this);
  }

  //----------------------------------GETTER/SETTER METHODS---------------------------//
  
  
  int getType(){
    return TYPE;
  }

  void setNote(String note, int loc) {
    this.phrase[loc] = note;
  }

  void setLength(int l) {
    pLength = l;
  }

  int getLength() {
    return pLength;
  }

  void setRepeat(int i) {
    repeat = i;
  }

  int getRepeat() {
    return repeat;
  }

  float getDuration(int i) {
    return durations[i];
  }

  void setDurations(float[] durations) {
    this.durations = durations;
    if (durations.length > 2) beat_val = durations[2];
    else beat_val = durations[0];
    soar_val = durations[0] + durations[1];
  }

  void setDuration(int i, float v) {
    durations[i] = v;
  }

  float[] getDurations() {
    return this.durations;
  }

  void setStarts(float[] starts) {
    this.starts = starts;
  }

  void setStates(int[] states) {
    if (states.length != this.states.length) System.out.println("Mismatch state length");
    this.states = states;
  }

  int[] getStates() {
    return states;
  }

  void setPhrase(String[] phrase) {
    this.phrase = phrase;
  }
  
  void setPhrase(int i, String n){
    this.phrase[i] = n;
  }
  
  void setPhraseNums(int[] p){
    this.phraseNums = p;
  }

  String[] getPhrase() {
    return this.phrase;
  }

  float getSoar() {
    return soar_val;
  }

  float getBeat() {
    return beat_val;
  }

  float[] getStarts() {
    return this.starts;
  } 

  void playPhrase() {
    //call music player or feed getPhrase output to musicPlayer... might be better
  }

  MusicModel getModel() {
    return mod;
  }

  //boolean[] getActiveModes() {
  //  return activeModes;
  //}

  //----------------------------------DRAWING FUNCTIONS---------------------------//
  
  boolean drawChar(int on){
    drawChar(on, false);
    return false;
  }
  
  boolean drawChar(int on, boolean motion) {

    //for (int i = 0; i < activeModes.length; i++) //ACTIVE MODES
    //{
    //  if (WTIME == 4)
    //    if (!activeModes[i]) {
    //      screen.fill(240);
    //    } else {
    //      if (modes[i]) screen.fill(set1[i]);
    //      else screen.fill(set2[i]);
    //    }    
    //  screen.rect(x+i*60, y, 20,20);
    //}

    textSize(30);
    fill(150);
    text(name, x, y);
    if (isSelected()) {
      screen.fill(onColor, 30);
      screen.ellipse(x,y,70,70);
      screen.fill(onColor);
    } else {
      screen.fill(offColor);
    }
    screen.ellipse(x, y, 45, 45);

    if (on > 0) {
      locX = (locX+50)% (screen.width - 200);
      if (locX < 300) {locX += 300 - locX; opacity = 150;} //opacity for musicScreen
    }
    

    if (on == 1) {
      boolean[] modes = mod.getModes();
      float noteShift0 = phraseNums[0]*2.5;
      int noteShift1 = 0;//-phraseNums[1]*2;

      if (modes[SOAR_BEAT] == SOAR) screen.fill(240);
      else screen.fill(50);
      screen.ellipse(locX, locY-noteShift0, 20, 20);

      int sign = 0;

      if (modes[RISE_FALL] == RISE) sign = -1;
      else sign = 1;

      if (modes[BIG_SMALL] == BIG) screen.ellipse(locX +10, locY+sign*50 - noteShift0, 20, 20);
      else screen.ellipse(locX+5, locY+sign*20-noteShift0, 20, 20);

      if (modes[OPEN_CLOSED] == CLOSED) screen.ellipse(locX+20, locY-noteShift0, 20, 20);
    } else if (on ==2) { //this beat is a rest
      if (mod.getModes()[SOAR_BEAT] == SOAR) screen.fill(240);
      else screen.fill(50);
      screen.rect(locX+10, y, 10, 10);
    }
    
    return false;
  }
}

//class MusicChar extends ModuleChar{
//  //notes, music modifying behavior
//  //
//  MusicChar(int x, int y)
//  {
//    this.x = x;
//    this.y = y;
//  }

//  void setPhrase(){
//  }

//  void getPhrase(){
//  }

//  void playPhrase(){
//  }

//}
